function initTimeline() {
	SimileAjax.History.enabled = false;
	var tl_el = document.getElementById("tl");
	eventSource1 = new Timeline.DefaultEventSource();
	var theme1 = Timeline.ClassicTheme.create();
	theme1.autoWidth = false;
	theme1.timeline_start = new Date(Date.UTC(2013, 4, 9));
	theme1.timeline_stop = new Date(Date.UTC(2013, 12, 30));

	d = new Date(Date.UTC(2013, 5, 9));
	var zones = [{
		start : "May 20 2013 00:00:00 GMT-0100",
		end : "Jun 08 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.HOUR
	}, {
		start : "Jun 09 2013 00:00:00 GMT-0100",
		end : "Jun 23 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.HOUR
	}, {
		start : "Jun 23 2013 00:00:00 GMT-0100",
		end : "Jul 01 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.HOUR
	}, {
		start : "Jul 01 2013 00:00:00 GMT-0100",
		end : "Aug 01 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.HOUR
	}, {
		start : "Aug 01 2013 00:00:00 GMT-0100",
		end : "Aug 18 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.HOUR
	}, {
		start : "Aug 18 2013 00:00:00 GMT-0100",
		end : "Sep 02 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.HOUR
	}, {
		start : "Sep 02 2013 00:00:00 GMT-0100",
		end : "Sep 10 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.HOUR
	}];

	var zones2 = [{
		start : "May 20 2013 00:00:00 GMT-0100",
		end : "Jun 09 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.DAY
	}, {
		start : "Jun 09 2013 00:00:00 GMT-0100",
		end : "Jun 23 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.DAY
	}, {
		start : "Jun 23 2013 00:00:00 GMT-0100",
		end : "Jul 01 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.DAY
	}, {
		start : "Jul 01 2013 00:00:00 GMT-0100",
		end : "Aug 01 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.DAY
	}, {
		start : "Aug 01 2013 00:00:00 GMT-0100",
		end : "Aug 18 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.DAY
	}, {
		start : "Aug 18 2013 00:00:00 GMT-0100",
		end : "Sep 02 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.DAY
	}, {
		start : "Sep 02 2013 00:00:00 GMT-0100",
		end : "Sep 10 2013 00:00:00 GMT-0100",
		magnify : 10,
		unit : Timeline.DateTime.DAY
	}];

	var bandInfos = [Timeline.createBandInfo({
		width : "20%",
		eventSource : eventSource1,
		date : d,
		intervalUnit : Timeline.DateTime.WEEK,
		intervalPixels : 200,
		layout : 'overview', // original, overview, detailed
		zones : zones2

	}), Timeline.createHotZoneBandInfo({
		zones : zones,
		width : "80%", // set to a minimum, autoWidth will then adjust
		intervalUnit : Timeline.DateTime.DAY,
		intervalPixels : 100,
		eventSource : eventSource1,
		date : d,
		theme : theme1,
		layout : 'original' // original, overview, detailed
	})];

	var theme = Timeline.ClassicTheme.create();
	theme.event.bubble.width = 250;
	theme.event.bubble.color = "#ff000";

	for (var i = 0; i < bandInfos.length; i++) {
		bandInfos[i].decorators = [new Timeline.SpanHighlightDecorator({
			startDate : "May 20 2013 00:00:00 GMT-0100",
			endDate : "Jun 09 2013 00:00:00 GMT-0100",
			color : "#1F2E4C", // set color explicitly
			opacity : 30,
			startLabel : "PRISM scandal breaks",
			endLabel : "",
			theme : theme
		}), new Timeline.SpanHighlightDecorator({
			startDate : "Jun 09 2013 00:00:00 GMT-0100",
			endDate : "Jun 23 2013 00:00:00 GMT-0100",
			color : "#1F2E4C", // set color explicitly
			opacity : 30,
			startLabel : "ID",
			endLabel : "",
			theme : theme
		}), new Timeline.SpanHighlightDecorator({
			startDate : "Jun 23 2013 00:00:00 GMT-0100",
			endDate : "Jul 01 2013 00:00:00 GMT-0100",
			color : "#1F2E4C", // set color explicitly
			opacity : 30,
			startLabel : "Moscow",
			endLabel : "",
			theme : theme
		}), new Timeline.SpanHighlightDecorator({
			startDate : "Jul 01 2013 00:00:00 GMT-0100",
			endDate : "Aug 01 2013 00:00:00 GMT-0100",
			color : "#1F2E4C", // set color explicitly
			opacity : 30,
			startLabel : "Morales",
			endLabel : "",
			theme : theme
		}), new Timeline.SpanHighlightDecorator({
			startDate : "Aug 01 2013 00:00:00 GMT-0100",
			endDate : "Aug 18 2013 00:00:00 GMT-0100",
			color : "#1F2E4C", // set color explicitly
			opacity : 30,
			startLabel : "asylum in Russia",
			endLabel : "",
			theme : theme
		}), new Timeline.SpanHighlightDecorator({
			startDate : "Aug 18 2013 00:00:00 GMT-0100",
			endDate : "Sep 02 2013 00:00:00 GMT-0100",
			color : "#1F2E4C", // set color explicitly
			opacity : 30,
			startLabel : "Miranda detained",
			endLabel : "",
			theme : theme
		}), new Timeline.SpanHighlightDecorator({
			startDate : "Sep 02 2013 00:00:00 GMT-0100",
			endDate : "Sep 10 2013 00:00:00 GMT-0100",
			color : "#1F2E4C", // set color explicitly
			opacity : 30,
			startLabel : "DEA and AT&T",
			endLabel : "",
			theme : theme
		})
		
		
		];
	}

	bandInfos[0].syncWith = 1;
	bandInfos[0].highlight = true;
	// create the Timeline
	tl = Timeline.create(tl_el, bandInfos, Timeline.HORIZONTAL);
}


function updateState(band) {
	var centerDate = band.getCenterVisibleDate();
	var minDate = band.getMinVisibleDate();
	var maxDate = band.getMaxVisibleDate();
	var citiesTag;
	var pointsMap = {};
	var evtSource = tl.getBand(1).getEventSource();
	var emoMap = {};
	var emos = [];
	var graphsToLoad = {};
	points = [];
	var nrEvents = 0;
	ga('send', 'event', 'Timeline', minDate, 'Overall', 1);
	var iterator = eventSource1.getEventIterator(tl.getBand(1).getMinVisibleDate(), tl.getBand(1).getMaxVisibleDate());
	while (iterator.hasNext()) {
		nrEvents ++;
		var evt = iterator.next();
		var evtTitle = evt.getProperty("title");

		var siteID = evt.getProperty("classname");
		var graph = evt.getProperty("graph");
		citiesTag = evt.getProperty("cities");
		var evtId = $.parseJSON(graph).id;
		graphsToLoad[evtId] = graph;
		var emo = evt.getProperty("emo");
		var parsedEmo = $.parseJSON(emo);
		
		//emo chart data
		for (var i = 0; i < parsedEmo.length; i++) {
			var key = parsedEmo[i].term;
			var value = parsedEmo[i].value;
			if (emoMap[key] == undefined)
				emoMap[key] = 1;
			else
				emoMap[key] = emoMap[key] + value;
		}
		
		//get city data
		var parsedCities = $.parseJSON(citiesTag);
		var features = parsedCities.features;
		for (var i = 0; i < features.length; i++) {
			var lat = features[i].city.lat;
			var lon = features[i].city.lon;
			var cityName = features[i].city.name;
			var articleTitle = features[i].city.title;

			if (pointsMap[cityName] == undefined) {
				//console.log ("undefined: " + cityName);
				var aTitle = {};
				aTitle[articleTitle] = 1;
				pointsMap[cityName] = 
				{'titles': aTitle,
				 'position': latLongToVector3(lon, lat, earthRadius, 2),
				 'orbit': latLongToVector3(lon, lat, earthRadius + 300, 2),
				 'latlon' : {'lat' : lat, 'lon': lon}}
				;
			}	else { // dodaj kraje v obstoječe članke
				var existing = pointsMap[cityName];
				var existingTitles = existing.titles;
				if (!(cityName in existingTitles))	existingTitles[articleTitle] = 1;
			}	
		}
	}
	
	if (nrEvents == 0) sigInst.emptyGraph();
	//draw flot chart
	drawChart(emoMap, emos);
	
	// load graphs
	for (var i in graphsToLoad) {
		loadGraph(graphsToLoad[i]);
	}
	for (var g in graphsMap) {
		if (graphsToLoad[g] == undefined && graphsMap[g] != undefined) {
			unloadGraph(graphsMap[g]);
		}
	}

	fa2Layout(3500);
	d = centerDate;
	var curr_date = centerDate.getDate();
	var curr_month = centerDate.getMonth() + 1;
	var curr_year = centerDate.getFullYear();
	var curr_hour = centerDate.getHours();
	var curr_minute = centerDate.getMinutes();
	dateDiv.innerHTML = "<p style = 'font-size:12px'>" + formatDate (centerDate) + "<p>";
	removePoints();
	showCities(pointsMap);
}

function formatDate (date) {
	var formatted = day_names[date.getDay()] + ", " + month_names[date.getMonth()] + " " + date.getFullYear() + "<br>" + doubleDigits(date.getDate()) + "h : " + doubleDigits(date.getMinutes()) + " min";
	return formatted; 
} 

function doubleDigits (s) {
	if (s.toString().length <= 1) return "0" + s;
	else return s;
}


function jumpTo (date) {
    tl.getBand(1).setCenterVisibleDate(Timeline.DateTime.parseGregorianDateTime(date));
}