var graphsMap = {};
var prevEvents = {};
var atlasAllowed = true;
var tl;
var sigInst;
var eventSource1;
var layersMap = {};
var projector = new THREE.Projector();
var points = [];
var orbit = [];
var can, ctx, step, steps = 0;
var timer = 0;
var rotateSpeed = 0.004;
var FOV = 45;
var NEAR = 1;
var FAR = 4000;
var locationPivot;
var pointsGeometry;
var pointObject3D = [];
var earthRadius = 1000;
var earthOriginVector = latLongToVector3(0, 0, earthRadius, 2);
var renderer = new THREE.WebGLRenderer();
var POS_X = 1800;
var POS_Y = 500;
var POS_Z = 1800;
var snowdenLoc = [
	{}
];
var playing = false;
var rotating = true;
var helpShown = true;
var mediaMap = {
	'Guardian' : timeline_data_guardian,
	'Business Insider' : timeline_data_businessinsider,
	'Washington Post' : timeline_data_washingtonpost,
	'Press.tv' : timeline_data_presstv,
	'BBC' : timeline_data_bbc,
	'Bloomberg' : timeline_data_bloomberg,
	'China Daily' : timeline_data_chinadaily,
	'Daily Mail' : timeline_data_dailymail,
	'Fox News' : timeline_data_fox,
	'Moscow Times' : timeline_data_moscowtimes,
	'Reuters' : timeline_data_reuters,
	'Xinhua' : timeline_data_xinhua,
	'RT' : timeline_data_rt

};
var jumpMap = {
	'PRISM scandal breaks' : new Date(Date.UTC(2013, 4, 20)),
	'Snowden identified' : new Date(Date.UTC(2013, 5, 9)),
	'Snowden in Moscow' : new Date(Date.UTC(2013, 5 ,23)),
	'Morales incident' : new Date(Date.UTC(2013, 6, 1)),
	'Asylum in Russia' : new Date(Date.UTC(2013, 7, 1)),
	'Miranda detained' : new Date(Date.UTC(2013, 7, 18)),
	'DEA and AT&T' : new Date(Date.UTC(2013, 8, 2)),
	'Encryption useless' : new Date(Date.UTC(2013, 8, 7))
};
var month_names = new Array ( );
month_names[month_names.length] = "January";
month_names[month_names.length] = "February";
month_names[month_names.length] = "March";
month_names[month_names.length] = "April";
month_names[month_names.length] = "May";
month_names[month_names.length] = "June";
month_names[month_names.length] = "July";
month_names[month_names.length] = "August";
month_names[month_names.length] = "September";
month_names[month_names.length] = "October";
month_names[month_names.length] = "November";
month_names[month_names.length] = "December";

var day_names = new Array ( );
day_names[day_names.length] = "Sunday";
day_names[day_names.length] = "Monday";
day_names[day_names.length] = "Tuesday";
day_names[day_names.length] = "Wednesday";
day_names[day_names.length] = "Thursday";
day_names[day_names.length] = "Friday";
day_names[day_names.length] = "Saturday";

var d;


var WIDTH = $(window).width();
var HEIGHT = $(window).height() * 0.95;
renderer.setSize(WIDTH, HEIGHT);
renderer.setClearColorHex(0x000000);
var mapDiv = document.getElementById("map");
mapDiv.appendChild(renderer.domElement);
var camera = new THREE.PerspectiveCamera(FOV, WIDTH / HEIGHT, NEAR, FAR);
camera.position.set(POS_X, POS_Y, POS_Z);
camera.lookAt(new THREE.Vector3(0, 0, 0));
var scene = new THREE.Scene();
var glowscene = new THREE.Scene();
glowcamera = new THREE.PerspectiveCamera(FOV, WIDTH / HEIGHT, NEAR, FAR);
glowcamera.position.set(POS_X, POS_Y, POS_Z);
glowcamera.lookAt(new THREE.Vector3(0, 0, 0));
scene.add(camera);
glowscene.add(glowcamera);
var tweenInterval = 2000;
var idx = 0;
var dateDiv;

var renderTargetParameters = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBFormat, stencilBufer: false };
renderTargetGlow = new THREE.WebGLRenderTarget( WIDTH, HEIGHT, renderTargetParameters );

//var saveGlowPass = new THREE.SavePass( new THREE.WebGLRenderTarget( SCREEN_WIDTH, SCREEN_HEIGHT, renderTargetParameters ) );

var effectFXAA = new THREE.ShaderPass( THREE.ShaderExtras[ "fxaa" ] );
effectFXAA.uniforms[ 'resolution' ].value.set( 1 / WIDTH, 1 / HEIGHT );

hblur = new THREE.ShaderPass( THREE.ShaderExtras[ "horizontalBlur" ] );
vblur = new THREE.ShaderPass( THREE.ShaderExtras[ "verticalBlur" ] );

var bluriness = 3;

hblur.uniforms[ 'h' ].value = bluriness / WIDTH;
vblur.uniforms[ 'v' ].value = bluriness / HEIGHT;

var renderModelGlow = new THREE.RenderPass( glowscene, glowcamera );

glowcomposer = new THREE.EffectComposer( renderer, renderTargetGlow );

glowcomposer.addPass( renderModelGlow );
//glowcomposer.addPass( saveGlowPass );
glowcomposer.addPass( hblur );
glowcomposer.addPass( vblur );
glowcomposer.addPass( hblur );
glowcomposer.addPass( vblur );
var finalshader = {
					uniforms: {
						tDiffuse: { type: "t", value: 0, texture: null },
						tGlow: { type: "t", value: 1, texture: null },
						//tLumi: { type: "t", value: 2, texture: null }
					},

					vertexShader: [
						"varying vec2 vUv;",

						"void main() {",

							"vUv = vec2( uv.x, 1.0 - uv.y );",
							"gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

						"}"
					].join("\n"),

					fragmentShader: [
						"uniform sampler2D tDiffuse;",
						"uniform sampler2D tGlow;",
						//"uniform sampler2D tLumi;",

						"varying vec2 vUv;",

						"void main() {",

							"vec4 texel = texture2D( tDiffuse, vUv );",
							"vec4 glow = texture2D( tGlow, vUv );",
							//"vec4 lumi = texture2D( tLumi, vUv );",
							"gl_FragColor = texel + vec4(0.5, 0.75, 1.0, 1.0) * glow * 2.0;",

						"}"
					].join("\n")
				};

finalshader.uniforms[ 'tGlow' ].texture = glowcomposer.renderTarget2;
//finalshader.uniforms[ 'tLumi' ].texture = saveGlowPass.renderTarget;
var renderModel = new THREE.RenderPass( scene, camera );
var finalPass = new THREE.ShaderPass( finalshader );
finalPass.needsSwap = true;
finalPass.renderToScreen = true;
renderTarget = new THREE.WebGLRenderTarget( WIDTH, HEIGHT, renderTargetParameters );
finalcomposer = new THREE.EffectComposer( renderer, renderTarget );
finalcomposer.addPass( renderModel );
finalcomposer.addPass( effectFXAA );
finalcomposer.addPass( finalPass );

$(function initialize() {
    dateDiv = document.getElementById("time");
	initTimeline();
	initSigma();
	initGlobe();
    themeSwitch();
    tl.getBand(1).addOnScrollListener ( updateState);
    var url = '.'; // The base url for image, icon and background image references in the data
    eventSource1.loadJSON(timeline_data_guardian, url);
    tl.layout(); // display the Timeline
});
	


function initSigma() {
	var sigRoot = document.getElementById('wordcloud');
	sigInst = sigma.init(sigRoot).drawingProperties({
		defaultLabelColor : '#ffffff',
		labelSize : 'proportional',
		defaultLabelBGColor : '#fff',
		defaultLabelHoverColor : '#000',
		labelThreshold : 0,
		defaultEdgeType : 'line'
	}).graphProperties({
		minNodeSize : 5,
		maxNodeSize : 18,
		minEdgeSize : 1.5,
		maxEdgeSize : 10
	}).mouseProperties({
		maxRatio : 10
	});
var greyColor = '#5c4473';
sigInst.bind('overnodes',function(event){
    var nodes = event.content;
    var neighbors = {};
    sigInst.iterEdges(function(e){
      if(nodes.indexOf(e.source)>=0 || nodes.indexOf(e.target)>=0){
        neighbors[e.source] = 1;
        neighbors[e.target] = 1;
      }
    }).iterNodes(function(n){
      if(!neighbors[n.id]){
        n.hidden = 1;
      }else{
        n.hidden = 0;
      }
    }).draw(2,2,2);
  }).bind('outnodes',function(){
    sigInst.iterEdges(function(e){
      e.hidden = 0;
    }).iterNodes(function(n){
      n.hidden = 0;
    }).draw(2,2,2);
  });
}

function initGlobe() {
	addPoints(points);
	addLights();
	addEarth();
	initCanvas();
	animate();
};



function addLights() {
	light = new THREE.DirectionalLight(0x3333ee, 0.5, 500);
	glowlight = new THREE.DirectionalLight(0x3333ee, 3.5, 500);
	scene.add(light);
	glowscene.add(glowlight);
	light.position.set(POS_X, POS_Y, POS_Z);
}

// add the earth
function addEarth() {
	var spGeo = new THREE.SphereGeometry(earthRadius, 50, 50);
	var planetTexture = THREE.ImageUtils.loadTexture("img/world-black2.jpg");
	var mat2 = new THREE.MeshPhongMaterial({
		map : planetTexture,
		perPixel : false,
		shininess : 0.21,
		opacity : 0.9,
		brightness:0.1
	});
	sp = new THREE.Mesh(spGeo, mat2);
	scene.add(sp);
}

function initCanvas () {
    can = document.getElementById("drawCanvas");
    can.width = WIDTH;
    can.height = HEIGHT;
    ctx = can.getContext("2d");
    ctx.fillStyle = "lightblue";
    ctx.font = "10pt Verdana";
    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
}










//=================== GLOBE *****************************
//=================== GLOBE *****************************
//=================== GLOBE *****************************
//=================== GLOBE *****************************


// RENDER LOOP

function animate() {
	requestAnimationFrame( animate );
	render();
}

function render() {
    camera.position = cameraPos;
    camera.lookAt( scene.position );
	light.position = camera.position;
	light.lookAt(scene.position);
	camera.updateMatrixWorld();
	
	glowcamera.position = cameraPos;
    glowcamera.lookAt( scene.position );
	glowlight.position = camera.position;
	glowlight.lookAt(scene.position);
	glowcamera.updateMatrixWorld();
	addGlobeLabels();
	
	//glowcomposer.render( 0.1 );
	//finalcomposer.render( 0.1 );
	
	renderer.render(scene, camera);
	if (toDisplayUnsorted.length > 1 && tweenAllowed && toDisplayUnsorted !=undefined) {
		if (rotating) tweener ();
	}
}


tweenAllowed = true;
var tweenIndex = 0;
var curveIndex = 0;
var cameraPos = latLongToVector3 (19, 45, earthRadius + 300, 0);
var nrSteps = 200;
var dLat,dLon,startLat,startLon,endLat,endLon;
var lonStep = 0.3;


function tweener () {
	if (tweenAllowed) {
		if ((toDisplayUnsorted.length > 1) && curveIndex == 0) {
			var start = toDisplayUnsorted[tweenIndex].latlon;
			if (tweenIndex == (toDisplayUnsorted.length -1)) secondIdx = 0;
			else secondIdx = tweenIndex + 1;
			var end   = toDisplayUnsorted[secondIdx].latlon;
			startLat = start.lat;//45;
			startLon = start.lon;//15;
			endLat = end.lat;//54;
			endLon = end.lon;//-25;
			dLat = (endLat - startLat) / nrSteps;
			dLon = (endLon - startLon) / nrSteps;	
			
			curveIndex++;
		}
	
		if (!(curveIndex == nrSteps)) { 
			curveIndex++;
			startLat += dLat;
			startLon += dLon;
			cameraPos = latLongToVector3 (startLon, startLat, earthRadius + 800, 0);
		}
		else {
			tweenIndex ++;
			if (tweenIndex == toDisplayUnsorted.length) tweenIndex = 0;
			curveIndex = 0;
		}
		
	}
	
}

function coordReached (lat1,lon1,lat2,lon2) {
	var lats = false;
	var lons = false;
	if ((lat2 - lat1) < 0.5) lats = true;
	if ((lon2 - lon1) < 0.5) lons = true;
	return lats && lons;
}

function showCities (pointsMap) {
	for (var city in pointsMap) {
		var titles = pointsMap[city].titles;
		var position = pointsMap[city].position;
		var entry = {
			"position" : position,
			"title" : pointsMap[city].titles,
			"name" : city,
			"orbit" : pointsMap[city].orbit,
			'latlon' : pointsMap[city].latlon
		};
		points.push(entry);
	}
	addPoints(points);
}

	if (points.length > 1) {
			var tweening = window.setInterval(function () {
			setUpTween(points[idx],points[idx+1], points[idx+1]);
			idx++;
			if (idx == points.length-1) idx = 0;
		}, tweenInterval) ;  
	}


//deproject
function toXYCoords(pos) {
	var mapDiv = document.getElementById("map");
	camera.updateMatrixWorld(); 
	var vector = projector.projectVector(pos.clone(), camera);
	vector.x = (vector.x + 1) / 2 * mapDiv.offsetWidth;
	vector.y = -(vector.y - 1) / 2 * mapDiv.offsetHeight;
	
	return vector;
}

function zComparator(a,b) {
  if (a.z < b.z)
     return 1;
  if (a.z> b.z)
    return -1;
  return 0;
}

function lonComparator(a,b) {
  if (a.lon < b.lon)
     return 1;
  if (a.lon > b.lon)
    return -1;
  return 0;
}

var toDisplay,toDisplayUnsorted;
var prevPointsLen = 0;

function addGlobeLabels() {
	tweenAllowed = false;
	ctx.clearRect(0, 0, can.width, can.height);
	toDisplay = [];
	toDisplayUnsorted = [];
	for (var i = 0; i < points.length; i++) {
		if (points[i].position != undefined) {
			var vector = toXYCoords (points[i].position);
			var oneLabel = {
				'position' : vector,
				'z' : vector.z,
				'city' : points[i].name,
				'titles' : points[i].title,
				'orbit' : points[i].orbit,
				'latlon' : points[i].latlon,
				'lon' : points[i].latlon.lon
			};
			toDisplayUnsorted.push(oneLabel);
			if (vector.z < 0.9989) { 
				toDisplay.push(oneLabel);
			}
		}
	}
	toDisplay.sort(zComparator);
	
	for (var la = 0; la < toDisplay.length; la ++) {
		var entry = toDisplay[la];
		var vector = entry.position;
		var maxX = 0;
		var lines = 0;

		
		var fontSizeTitle = Math.abs((vector.z - 0.9989) * 1000);
		ctx.font = fontSizeTitle * 8 + "pt Verdana";
		for (var oneTitle in entry.titles) {
			var tDim = ctx.measureText(oneTitle); 
			if (tDim.width > maxX) maxX = tDim.width;
			lines++;
		}
		var maxY = lines * 16 * fontSizeTitle + fontSizeTitle * 24;
		maxX *= 1.1;
		ctx.save();
		ctx.textBaseline = 'top';
		ctx.translate(vector.x, vector.y);
		ctx.beginPath();
		ctx.lineWidth="1";
		ctx.fillStyle="black";
		ctx.strokeStyle="lightblue";
		ctx.fillRect (0, 0, maxX, maxY);
		ctx.rect (0, 0, maxX, maxY);
		ctx.stroke();
		ctx.fillStyle = "white";
		ctx.font = "bold " + fontSizeTitle * 12 + "pt Verdana";
		ctx.fillText(entry.city, 2, 2);
		ctx.fillStyle = "lightblue";
		ctx.font = fontSizeTitle * 8 + "pt Verdana";
		var ycoord = 22 * fontSizeTitle;
		for (var oneTitle in entry.titles) {
			ctx.fillText( oneTitle, 2, ycoord);
			ycoord+=16 * fontSizeTitle;
		}
		ctx.restore();
	}
	var snowdenLabel = {
		'position' : toXYCoords (latLongToVector3 (15,45,700,0)),
		'z' : toXYCoords (latLongToVector3 (15,45,700,0)).z,
		'city' : "points[i].name",
		'titles' : "snowden",
		'orbit' : "",
		'latlon' : {'lat' : 45, 'lon' : 15},
		'lon' : 15
	};
	toDisplay.push(snowdenLabel);
	toDisplayUnsorted.push(oneLabel);
	if (toDisplayUnsorted.length != prevPointsLen) {
		tweenIndex = 0;
	}
	prevPointsLen = toDisplayUnsorted.length;
	tweenAllowed = true;
	toDisplayUnsorted.sort(lonComparator);
}




function removePoints() {
	scene.remove(pointGeometry);
}

function addPoints(data) {
	var geom = new THREE.Geometry();
	for (var i = 0; i < data.length; i++) {
		if (data[i].position != undefined) {
			var cube = addPoint(data[i]);
			THREE.GeometryUtils.merge(geom, cube);
		}
	}
	pointGeometry = new THREE.Mesh(geom, new THREE.MeshFaceMaterial());
	var glowGeometry = new THREE.Mesh(geom, new THREE.MeshFaceMaterial());
	scene.add(pointGeometry);
	glowscene.add(glowGeometry);
}

function addPoint(position) {
	var geom = new THREE.Geometry();
	var cubeMat = new THREE.MeshLambertMaterial({
		color : 0xFF0000,
		opacity : 1,
		emissive : 0xffffff
	});
	var cube = new THREE.Mesh(new THREE.CubeGeometry(5, 5, 5, 1, 1, 1, cubeMat));
	cube.position = position.position;
	cube.lookAt(new THREE.Vector3(0, 0, 0));
	THREE.GeometryUtils.merge(geom, cube);
	var total = new THREE.Mesh(geom, new THREE.MeshFaceMaterial());
	return total;
}



