var timeline_data_chinadaily = {
'dateTimeFormat': 'Java RFC3999',
'events' : [
        {'start': 'Aug 06 2013 07:11:00 GMT-0100', 
        'title': 'Snowden saga',
        'classname': 'chinadaily',
        'graph': '{"id":0,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-08/06/content_16873248.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Aug 02 2013 09:13:00 GMT-0100', 
        'title': 'Snowden safe and fine after getting permit to enter Russia',
        'classname': 'chinadaily',
        'graph': '{"id":1,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/02/content_16867921.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Russia'

        },
        {'start': 'Aug 02 2013 07:20:00 GMT-0100', 
        'title': 'US rethinking Putin summit after Snowden move',
        'classname': 'chinadaily',
        'graph': '{"id":2,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/02/content_16864083.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Aug 01 2013 09:08:00 GMT-0100', 
        'title': 'Snowden granted 1 yrs temporary asylum in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":3,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"50","size":2,"x":"43"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"13","size":2,"x":"30"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"whistleblower","value":1},{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/01/content_16863517.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 29 2013 02:08:00 GMT-0100', 
        'title': 'Latest US-China talks should smooth the way',
        'classname': 'chinadaily',
        'graph': '{"id":4,"edges":[{"id":"United StatesEconomic Dialogue","weight":20,"source":"United States","target":"Economic Dialogue","label":""},{"id":"United StatesChina","weight":20,"source":"United States","target":"China","label":""},{"id":"Economic DialogueChina","weight":20,"source":"Economic Dialogue","target":"China","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"47","size":2,"x":"43"},{"id":"Economic Dialogue","color":"rgb(102,153,255)","label":"Economic Dialogue","y":"97","size":2,"x":"1"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"56","size":2,"x":"39"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/29/content_16847720.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Economic Dialogue, China'

        },
        {'start': 'Jul 28 2013 04:36:00 GMT-0100', 
        'title': 'No time limit for Snowdens stay: Russia',
        'classname': 'chinadaily',
        'graph': '{"id":5,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"34","size":2,"x":"11"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"95","size":2,"x":"36"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"66","size":2,"x":"65"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[{"city":{"title":"No time limit for Snowdens stay: Russia","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/28/content_16842455.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jul 27 2013 08:30:00 GMT-0100', 
        'title': 'Snowden faces another 6 months in limbo: official',
        'classname': 'chinadaily',
        'graph': '{"id":6,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"86","size":2,"x":"79"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"58","size":2,"x":"99"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden faces another 6 months in limbo: official","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/27/content_16839758.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden'

        },
        {'start': 'Sep 04 2013 09:57:00 GMT-0100', 
        'title': 'Putin outlines stances on Syria, relations with US',
        'classname': 'chinadaily',
        'graph': '{"id":7,"edges":[{"id":"United StatesSyria","weight":20,"source":"United States","target":"Syria","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"United StatesVladimir Putin","weight":20,"source":"United States","target":"Vladimir Putin","label":""},{"id":"SyriaEdward Snowden","weight":20,"source":"Syria","target":"Edward Snowden","label":""},{"id":"SyriaRussia","weight":20,"source":"Syria","target":"Russia","label":""},{"id":"SyriaVladimir Putin","weight":20,"source":"Syria","target":"Vladimir Putin","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVladimir Putin","weight":20,"source":"Edward Snowden","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"62","size":2,"x":"88"},{"id":"Syria","color":"rgb(102,153,255)","label":"Syria","y":"96","size":2,"x":"18"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"18","size":2,"x":"16"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"14","size":2,"x":"69"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"76","size":2,"x":"12"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-09/04/content_16944714.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Syria, Edward Snowden, Russia, Vladimir Putin'

        },
        {'start': 'Sep 03 2013 03:35:00 GMT-0100', 
        'title': 'Brazil says US spying revelations unacceptable',
        'classname': 'chinadaily',
        'graph': '{"id":8,"edges":[],"nodes":[]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-09/03/content_16941044.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Brazil'

        },
        {'start': 'Aug 20 2013 08:00:00 GMT-0100', 
        'title': 'Prince William and Kate release family snapshots',
        'classname': 'chinadaily',
        'graph': '{"id":9,"edges":[{"id":"Prince WilliamPrince George","weight":20,"source":"Prince William","target":"Prince George","label":""}],"nodes":[{"id":"Prince William","color":"rgb(102,153,255)","label":"Prince William","y":"73","size":2,"x":"76"},{"id":"Prince George","color":"rgb(102,153,255)","label":"Prince George","y":"97","size":2,"x":"67"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/20/content_16906075.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Prince William, Prince George'

        },
        {'start': 'Aug 20 2013 09:38:00 GMT-0100', 
        'title': 'Prince William and Kate release family snapshots',
        'classname': 'chinadaily',
        'graph': '{"id":10,"edges":[{"id":"Prince WilliamPrince George","weight":20,"source":"Prince William","target":"Prince George","label":""}],"nodes":[{"id":"Prince William","color":"rgb(102,153,255)","label":"Prince William","y":"49","size":2,"x":"11"},{"id":"Prince George","color":"rgb(102,153,255)","label":"Prince George","y":"50","size":2,"x":"26"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/entertainment/2013-08/20/content_16906679.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Prince William, Prince George'

        },
        {'start': 'Aug 12 2013 07:27:00 GMT-0100', 
        'title': 'Snowdens father gets visa, to leave for Russia soon',
        'classname': 'chinadaily',
        'graph': '{"id":11,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"10","size":2,"x":"14"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"77","size":2,"x":"72"}]}',
        'emo': '[{"term":"surveillance","value":1},{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/12/content_16886385.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Aug 10 2013 09:14:00 GMT-0100', 
        'title': 'Chilly talks expected for US, Russia',
        'classname': 'chinadaily',
        'graph': '{"id":12,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/10/content_16884966.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Syria, Russia'

        },
        {'start': 'Aug 09 2013 09:27:00 GMT-0100', 
        'title': 'Putin-Obama meeting at G20 summit not planned',
        'classname': 'chinadaily',
        'graph': '{"id":13,"edges":[{"id":"Barack ObamaKremlin","weight":20,"source":"Barack Obama","target":"Kremlin","label":""},{"id":"Barack ObamaRussia","weight":20,"source":"Barack Obama","target":"Russia","label":""},{"id":"Barack ObamaVladimir Putin","weight":20,"source":"Barack Obama","target":"Vladimir Putin","label":""},{"id":"KremlinRussia","weight":20,"source":"Kremlin","target":"Russia","label":""},{"id":"KremlinVladimir Putin","weight":20,"source":"Kremlin","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"28","size":2,"x":"40"},{"id":"Kremlin","color":"rgb(102,153,255)","label":"Kremlin","y":"48","size":2,"x":"12"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"46","size":2,"x":"38"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"4","size":2,"x":"55"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/09/content_16884351.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Barack Obama, Kremlin, Russia, Vladimir Putin'

        },
        {'start': 'Aug 09 2013 09:30:00 GMT-0100', 
        'title': 'Scrapped summit',
        'classname': 'chinadaily',
        'graph': '{"id":14,"edges":[{"id":"United StatesBarack Obama","weight":20,"source":"United States","target":"Barack Obama","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"United StatesVladimir Putin","weight":20,"source":"United States","target":"Vladimir Putin","label":""},{"id":"Barack ObamaRussia","weight":20,"source":"Barack Obama","target":"Russia","label":""},{"id":"Barack ObamaVladimir Putin","weight":20,"source":"Barack Obama","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"81","size":2,"x":"48"},{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"27","size":2,"x":"25"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"7","size":2,"x":"45"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"46","size":2,"x":"64"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-08/09/content_16882260.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Barack Obama, Russia, Vladimir Putin'

        },
        {'start': 'Aug 08 2013 09:51:00 GMT-0100', 
        'title': 'Snowden case not worth hurting Russia-US relations',
        'classname': 'chinadaily',
        'graph': '{"id":15,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"51","size":2,"x":"15"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"84","size":2,"x":"46"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"16","size":2,"x":"63"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden case not worth hurting Russia-US relations","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/08/content_16881358.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Aug 08 2013 09:42:00 GMT-0100', 
        'title': 'Obama cancels Putin talks over Snowden asylum offer',
        'classname': 'chinadaily',
        'graph': '{"id":16,"edges":[],"nodes":[]}',
        'emo': '[{"term":"asylum","value":1}]',
        'cities': '{"features":[{"city":{"title":"Obama cancels Putin talks over Snowden asylum offer","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/08/content_16879608.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow'

        },
        {'start': 'Aug 07 2013 09:39:00 GMT-0100', 
        'title': 'Obama cancels meeting with Russias Putin:official',
        'classname': 'chinadaily',
        'graph': '{"id":17,"edges":[{"id":"MoscowBarack Obama","weight":20,"source":"Moscow","target":"Barack Obama","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"MoscowVladimir Putin","weight":20,"source":"Moscow","target":"Vladimir Putin","label":""},{"id":"Barack ObamaRussia","weight":20,"source":"Barack Obama","target":"Russia","label":""},{"id":"Barack ObamaVladimir Putin","weight":20,"source":"Barack Obama","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"96","size":2,"x":"96"},{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"2","size":2,"x":"77"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"82","size":2,"x":"44"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"68","size":2,"x":"76"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Obama cancels meeting with Russias Putin:official","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/07/content_16878646.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Barack Obama, Russia, Vladimir Putin'

        },
        {'start': 'Aug 07 2013 09:51:00 GMT-0100', 
        'title': 'Obama going to G20 in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":18,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/07/content_16876515.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Russia'

        },
        {'start': 'Aug 07 2013 05:41:00 GMT-0100', 
        'title': 'US, Russian ministers to meet over differences',
        'classname': 'chinadaily',
        'graph': '{"id":19,"edges":[{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"United StatesState Department","weight":20,"source":"United States","target":"State Department","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenState Department","weight":20,"source":"Edward Snowden","target":"State Department","label":""},{"id":"RussiaState Department","weight":20,"source":"Russia","target":"State Department","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"63","size":2,"x":"76"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"1","size":2,"x":"62"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"87","size":2,"x":"4"},{"id":"State Department","color":"rgb(102,153,255)","label":"State Department","y":"64","size":2,"x":"50"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/07/content_16875619.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Edward Snowden, Russia, State Department'

        },
        {'start': 'Jul 26 2013 11:06:00 GMT-0100', 
        'title': 'Latest round of talks smoothes the path',
        'classname': 'chinadaily',
        'graph': '{"id":20,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/epaper/2013-07/26/content_16835853.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 26 2013 09:13:00 GMT-0100', 
        'title': 'Russia not to hand over Snowden to US: Kremlin',
        'classname': 'chinadaily',
        'graph': '{"id":21,"edges":[{"id":"Dmitry PeskovEdward Snowden","weight":20,"source":"Dmitry Peskov","target":"Edward Snowden","label":""},{"id":"Dmitry PeskovRussia","weight":20,"source":"Dmitry Peskov","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Dmitry Peskov","color":"rgb(102,153,255)","label":"Dmitry Peskov","y":"75","size":2,"x":"68"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"54","size":2,"x":"56"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"27","size":2,"x":"16"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/26/content_16839154.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Dmitry Peskov, Kremlin, Edward Snowden, Russia'

        },
        {'start': 'Jul 25 2013 08:00:00 GMT-0100', 
        'title': 'Snowdens hopes of leaving airport dashed',
        'classname': 'chinadaily',
        'graph': '{"id":22,"edges":[{"id":"SheremetyevoMoscow","weight":20,"source":"Sheremetyevo","target":"Moscow","label":""},{"id":"SheremetyevoEdward Snowden","weight":20,"source":"Sheremetyevo","target":"Edward Snowden","label":""},{"id":"SheremetyevoRussia","weight":20,"source":"Sheremetyevo","target":"Russia","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Sheremetyevo","color":"rgb(102,153,255)","label":"Sheremetyevo","y":"41","size":2,"x":"85"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"96","size":2,"x":"21"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"55","size":2,"x":"73"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"36","size":2,"x":"21"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowdens hopes of leaving airport dashed","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/25/content_16827394.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Sheremetyevo, Edward Snowden, Russia'

        },
        {'start': 'Jul 25 2013 09:18:00 GMT-0100', 
        'title': 'Moscow cannot extradite Snowden:human rights chief',
        'classname': 'chinadaily',
        'graph': '{"id":23,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"89","size":2,"x":"63"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"96","size":2,"x":"57"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[{"city":{"title":"Moscow cannot extradite Snowden:human rights chief","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/25/content_16832778.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jul 25 2013 08:04:00 GMT-0100', 
        'title': 'US wants Russia to return Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":24,"edges":[{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"85","size":2,"x":"10"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"27","size":2,"x":"86"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"68","size":2,"x":"32"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/25/content_16832538.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Edward Snowden, Russia'

        },
        {'start': 'Jul 24 2013 11:26:00 GMT-0100', 
        'title': 'Snowden is not planning to leave Russia for now - lawyer',
        'classname': 'chinadaily',
        'graph': '{"id":25,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"1","size":2,"x":"45"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"43","size":2,"x":"31"}]}',
        'emo': '[{"term":"asylum","value":1},{"term":"surveillance","value":1},{"term":"lawyer","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/24/content_16826393.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 24 2013 08:36:00 GMT-0100', 
        'title': 'Report: Snowden has document to enter Russia',
        'classname': 'chinadaily',
        'graph': '{"id":26,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"15","size":2,"x":"43"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"96","size":2,"x":"94"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"20","size":2,"x":"37"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Report: Snowden has document to enter Russia","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/24/content_16825970.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jul 23 2013 09:36:00 GMT-0100', 
        'title': 'Snowden hopes to leave airport by Wednesday',
        'classname': 'chinadaily',
        'graph': '{"id":27,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"5","size":2,"x":"31"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"30","size":2,"x":"29"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"91","size":2,"x":"51"}]}',
        'emo': '[{"term":"press","value":1},{"term":"surveillance","value":1},{"term":"lawyer","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden hopes to leave airport by Wednesday","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/23/content_16815380.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jul 22 2013 08:23:00 GMT-0100', 
        'title': 'A shot in the arm for Sino-US ties',
        'classname': 'chinadaily',
        'graph': '{"id":28,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/22/content_16810105.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 22 2013 08:23:00 GMT-0100', 
        'title': 'A shot in the arm for Sino-US ties',
        'classname': 'chinadaily',
        'graph': '{"id":29,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/22/content_16809093.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 20 2013 07:36:00 GMT-0100', 
        'title': 'Obama to attend G20 meeting in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":30,"edges":[{"id":"Barack ObamaRussia","weight":20,"source":"Barack Obama","target":"Russia","label":""},{"id":"White HouseEdward Snowden","weight":20,"source":"White House","target":"Edward Snowden","label":""}],"nodes":[{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"85","size":2,"x":"32"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"91","size":2,"x":"20"},{"id":"White House","color":"rgb(102,153,255)","label":"White House","y":"51","size":2,"x":"0"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"4","size":2,"x":"48"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/20/content_16804278.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Barack Obama, White House, Edward Snowden, Russia'

        },
        {'start': 'Jul 19 2013 01:52:00 GMT-0100', 
        'title': 'Apple, Google urge surveillance disclosure',
        'classname': 'chinadaily',
        'graph': '{"id":31,"edges":[{"id":"FacebookGoogle","weight":20,"source":"Facebook","target":"Google","label":""}],"nodes":[{"id":"Facebook","color":"rgb(102,153,255)","label":"Facebook","y":"44","size":2,"x":"92"},{"id":"Google","color":"rgb(102,153,255)","label":"Google","y":"47","size":2,"x":"16"}]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/19/content_16799897.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Google, Facebook'

        },
        {'start': 'Jul 19 2013 07:45:00 GMT-0100', 
        'title': 'Obama weighs canceling Moscow talks with Putin',
        'classname': 'chinadaily',
        'graph': '{"id":32,"edges":[{"id":"MoscowBarack Obama","weight":20,"source":"Moscow","target":"Barack Obama","label":""},{"id":"MoscowWhite House","weight":20,"source":"Moscow","target":"White House","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"MoscowVladimir Putin","weight":20,"source":"Moscow","target":"Vladimir Putin","label":""},{"id":"Barack ObamaWhite House","weight":20,"source":"Barack Obama","target":"White House","label":""},{"id":"Barack ObamaRussia","weight":20,"source":"Barack Obama","target":"Russia","label":""},{"id":"Barack ObamaVladimir Putin","weight":20,"source":"Barack Obama","target":"Vladimir Putin","label":""},{"id":"White HouseRussia","weight":20,"source":"White House","target":"Russia","label":""},{"id":"White HouseVladimir Putin","weight":20,"source":"White House","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"46","size":2,"x":"12"},{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"27","size":2,"x":"81"},{"id":"White House","color":"rgb(102,153,255)","label":"White House","y":"36","size":2,"x":"70"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"46","size":2,"x":"28"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"72","size":2,"x":"28"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Obama weighs canceling Moscow talks with Putin","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/19/content_16797019.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Barack Obama, White House, Russia, Vladimir Putin'

        },
        {'start': 'Jul 18 2013 03:41:00 GMT-0100', 
        'title': 'US Olympic body rejects boycott call over Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":33,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"48","size":2,"x":"16"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"38","size":2,"x":"49"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/18/content_16794988.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 18 2013 08:05:00 GMT-0100', 
        'title': 'Putin puts US ties above Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":34,"edges":[{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"United StatesVladimir Putin","weight":20,"source":"United States","target":"Vladimir Putin","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVladimir Putin","weight":20,"source":"Edward Snowden","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"78","size":2,"x":"92"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"6","size":2,"x":"6"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"53","size":2,"x":"72"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"11","size":2,"x":"45"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/18/content_16791907.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Edward Snowden, Russia, Vladimir Putin'

        },
        {'start': 'Jul 18 2013 03:55:00 GMT-0100', 
        'title': 'Snowden may apply for Russian citizenship',
        'classname': 'chinadaily',
        'graph': '{"id":35,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"22","size":2,"x":"55"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"26","size":2,"x":"78"}]}',
        'emo': '[{"term":"surveillance","value":1},{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/18/content_16795083.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 17 2013 08:43:00 GMT-0100', 
        'title': 'Snowden has no plans to leave Russia soon-lawyer',
        'classname': 'chinadaily',
        'graph': '{"id":36,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"15","size":2,"x":"43"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"42","size":2,"x":"61"}]}',
        'emo': '[{"term":"asylum","value":1},{"term":"surveillance","value":1},{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/17/content_16790640.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 17 2013 08:43:00 GMT-0100', 
        'title': 'Russia-US ties more important than Snowden: Putin',
        'classname': 'chinadaily',
        'graph': '{"id":37,"edges":[{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"United StatesVladimir Putin","weight":20,"source":"United States","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"31","size":2,"x":"49"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"51","size":2,"x":"17"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"31","size":2,"x":"67"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/17/content_16790644.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Russia, Vladimir Putin'

        },
        {'start': 'Jul 17 2013 07:45:00 GMT-0100', 
        'title': 'Merkels rivals go on attack over US spying',
        'classname': 'chinadaily',
        'graph': '{"id":38,"edges":[{"id":"Edward SnowdenAngela Merkel","weight":20,"source":"Edward Snowden","target":"Angela Merkel","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"55","size":2,"x":"70"},{"id":"Angela Merkel","color":"rgb(102,153,255)","label":"Angela Merkel","y":"68","size":2,"x":"41"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/17/content_16786119.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Angela Merkel'

        },
        {'start': 'Jul 17 2013 10:10:00 GMT-0100', 
        'title': 'Obamas trouble',
        'classname': 'chinadaily',
        'graph': '{"id":39,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/17/content_16787259.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 16 2013 08:03:00 GMT-0100', 
        'title': 'Snowden officially asks for asylum in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":40,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"86","size":2,"x":"3"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"60","size":2,"x":"56"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/16/content_16784848.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 16 2013 05:00:00 GMT-0100', 
        'title': 'Snowden to leave Russia when opportunity emerges: Putin',
        'classname': 'chinadaily',
        'graph': '{"id":41,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVladimir Putin","weight":20,"source":"Edward Snowden","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"34","size":2,"x":"12"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"65","size":2,"x":"95"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"35","size":2,"x":"53"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/16/content_16780194.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia, Vladimir Putin'

        },
        {'start': 'Jul 16 2013 01:32:00 GMT-0100', 
        'title': 'Spain apologizes to Bolivia for plane delay',
        'classname': 'chinadaily',
        'graph': '{"id":42,"edges":[{"id":"Evo MoralesSpain","weight":20,"source":"Evo Morales","target":"Spain","label":""},{"id":"Evo MoralesEdward Snowden","weight":20,"source":"Evo Morales","target":"Edward Snowden","label":""},{"id":"Evo MoralesBolivia","weight":20,"source":"Evo Morales","target":"Bolivia","label":""},{"id":"SpainEdward Snowden","weight":20,"source":"Spain","target":"Edward Snowden","label":""},{"id":"SpainBolivia","weight":20,"source":"Spain","target":"Bolivia","label":""},{"id":"Edward SnowdenBolivia","weight":20,"source":"Edward Snowden","target":"Bolivia","label":""}],"nodes":[{"id":"Evo Morales","color":"rgb(102,153,255)","label":"Evo Morales","y":"81","size":2,"x":"54"},{"id":"Spain","color":"rgb(102,153,255)","label":"Spain","y":"36","size":2,"x":"86"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"61","size":2,"x":"97"},{"id":"Bolivia","color":"rgb(102,153,255)","label":"Bolivia","y":"83","size":2,"x":"96"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/16/content_16782625.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Evo Morales, Spain, Edward Snowden, Bolivia'

        },
        {'start': 'Jul 15 2013 08:25:00 GMT-0100', 
        'title': 'Snowden says he wont release harmful US data',
        'classname': 'chinadaily',
        'graph': '{"id":43,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/15/content_16775410.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Jul 15 2013 07:13:00 GMT-0100', 
        'title': 'Indeed a fruitful outcome',
        'classname': 'chinadaily',
        'graph': '{"id":44,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/15/content_16775349.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 12 2013 10:01:00 GMT-0100', 
        'title': 'Snowden meets Russian human rights groups',
        'classname': 'chinadaily',
        'graph': '{"id":45,"edges":[{"id":"SheremetyevoMoscow","weight":20,"source":"Sheremetyevo","target":"Moscow","label":""},{"id":"SheremetyevoRussia","weight":20,"source":"Sheremetyevo","target":"Russia","label":""},{"id":"SheremetyevoVyacheslav Nikonov","weight":20,"source":"Sheremetyevo","target":"Vyacheslav Nikonov","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"MoscowVyacheslav Nikonov","weight":20,"source":"Moscow","target":"Vyacheslav Nikonov","label":""},{"id":"RussiaVyacheslav Nikonov","weight":20,"source":"Russia","target":"Vyacheslav Nikonov","label":""},{"id":"United StatesMoscow","weight":20,"source":"United States","target":"Moscow","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""}],"nodes":[{"id":"Sheremetyevo","color":"rgb(102,153,255)","label":"Sheremetyevo","y":"57","size":2,"x":"14"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"99","size":4,"x":"10"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"14","size":2,"x":"45"},{"id":"Vyacheslav Nikonov","color":"rgb(102,153,255)","label":"Vyacheslav Nikonov","y":"31","size":2,"x":"84"},{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"92","size":2,"x":"21"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"77","size":2,"x":"55"}]}',
        'emo': '[{"term":"asylum","value":1},{"term":"illegal","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden meets Russian human rights groups","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/12/content_16769839.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Moscow, Sheremetyevo, Edward Snowden, Russia, Vyacheslav Nikonov'

        },
        {'start': 'Jul 12 2013 03:16:00 GMT-0100', 
        'title': 'Snowden to meet with activists, lawyers',
        'classname': 'chinadaily',
        'graph': '{"id":46,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"49","size":2,"x":"7"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"52","size":2,"x":"92"}]}',
        'emo': '[{"term":"surveillance","value":1},{"term":"lawyer","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/12/content_16768415.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 12 2013 02:54:00 GMT-0100', 
        'title': 'China rejects US criticism over HKs handling of Snowden case',
        'classname': 'chinadaily',
        'graph': '{"id":47,"edges":[{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""},{"id":"Hong KongChina","weight":20,"source":"Hong Kong","target":"China","label":""},{"id":"Edward SnowdenChina","weight":20,"source":"Edward Snowden","target":"China","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"51","size":2,"x":"41"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"96","size":2,"x":"57"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"95","size":2,"x":"47"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"China rejects US criticism over HKs handling of Snowden case","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/12/content_16768339.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, Edward Snowden, China'

        },
        {'start': 'Jul 12 2013 02:09:00 GMT-0100', 
        'title': 'Key dialogue one step further in redefining China-US ties',
        'classname': 'chinadaily',
        'graph': '{"id":48,"edges":[{"id":"United StatesChina","weight":20,"source":"United States","target":"China","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"1","size":2,"x":"77"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"84","size":2,"x":"96"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Key dialogue one step further in redefining China-US ties","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/12/content_16768076.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , China'

        },
        {'start': 'Jul 09 2013 03:38:00 GMT-0100', 
        'title': 'Dont let cyber security overshadow China-US talk',
        'classname': 'chinadaily',
        'graph': '{"id":49,"edges":[{"id":"United StatesChina","weight":20,"source":"United States","target":"China","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"0","size":2,"x":"14"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"90","size":2,"x":"34"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Dont let cyber security overshadow China-US talk","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/09/content_16752600.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , China'

        },
        {'start': 'Jul 09 2013 04:40:00 GMT-0100', 
        'title': 'Dont let cyber security overshadow key China-US dialogue',
        'classname': 'chinadaily',
        'graph': '{"id":50,"edges":[{"id":"United StatesChina","weight":20,"source":"United States","target":"China","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"21","size":2,"x":"71"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"37","size":2,"x":"33"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Dont let cyber security overshadow key China-US dialogue","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/09/content_16753038.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , China'

        },
        {'start': 'Jul 09 2013 05:04:00 GMT-0100', 
        'title': 'Cyber, trade, relationship building top issues at S&ED talks',
        'classname': 'chinadaily',
        'graph': '{"id":51,"edges":[{"id":"Economic DialogueChina","weight":20,"source":"Economic Dialogue","target":"China","label":""}],"nodes":[{"id":"Economic Dialogue","color":"rgb(102,153,255)","label":"Economic Dialogue","y":"48","size":2,"x":"69"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"49","size":2,"x":"15"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/09/content_16753180.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Economic Dialogue, China'

        },
        {'start': 'Jul 08 2013 08:01:00 GMT-0100', 
        'title': 'China expects new round of dialogue with US',
        'classname': 'chinadaily',
        'graph': '{"id":52,"edges":[{"id":"Economic DialogueChina","weight":20,"source":"Economic Dialogue","target":"China","label":""}],"nodes":[{"id":"Economic Dialogue","color":"rgb(102,153,255)","label":"Economic Dialogue","y":"50","size":2,"x":"88"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"99","size":2,"x":"2"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-07/08/content_16744738.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Economic Dialogue, China'

        },
        {'start': 'Jul 07 2013 09:27:00 GMT-0100', 
        'title': 'Bolivia latest country to offer asylum to Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":53,"edges":[{"id":"VenezuelaEdward Snowden","weight":20,"source":"Venezuela","target":"Edward Snowden","label":""},{"id":"VenezuelaNicaragua","weight":20,"source":"Venezuela","target":"Nicaragua","label":""},{"id":"VenezuelaBolivia","weight":20,"source":"Venezuela","target":"Bolivia","label":""},{"id":"Edward SnowdenNicaragua","weight":20,"source":"Edward Snowden","target":"Nicaragua","label":""},{"id":"Edward SnowdenBolivia","weight":20,"source":"Edward Snowden","target":"Bolivia","label":""},{"id":"NicaraguaBolivia","weight":20,"source":"Nicaragua","target":"Bolivia","label":""}],"nodes":[{"id":"Venezuela","color":"rgb(102,153,255)","label":"Venezuela","y":"4","size":2,"x":"45"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"33","size":2,"x":"39"},{"id":"Nicaragua","color":"rgb(102,153,255)","label":"Nicaragua","y":"27","size":2,"x":"4"},{"id":"Bolivia","color":"rgb(102,153,255)","label":"Bolivia","y":"74","size":2,"x":"31"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":2}]',
        'cities': '{"features":[{"city":{"title":"Bolivia latest country to offer asylum to Snowden","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/07/content_16742293.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Venezuela, Edward Snowden, Nicaragua, Bolivia'

        },
        {'start': 'Jul 07 2013 05:13:00 GMT-0100', 
        'title': '3 Latin American leaders offer Snowden asylum',
        'classname': 'chinadaily',
        'graph': '{"id":54,"edges":[{"id":"VenezuelaEdward Snowden","weight":20,"source":"Venezuela","target":"Edward Snowden","label":""},{"id":"VenezuelaNicaragua","weight":20,"source":"Venezuela","target":"Nicaragua","label":""},{"id":"VenezuelaBolivia","weight":20,"source":"Venezuela","target":"Bolivia","label":""},{"id":"Edward SnowdenNicaragua","weight":20,"source":"Edward Snowden","target":"Nicaragua","label":""},{"id":"Edward SnowdenBolivia","weight":20,"source":"Edward Snowden","target":"Bolivia","label":""},{"id":"NicaraguaBolivia","weight":20,"source":"Nicaragua","target":"Bolivia","label":""}],"nodes":[{"id":"Venezuela","color":"rgb(102,153,255)","label":"Venezuela","y":"20","size":2,"x":"17"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"6","size":2,"x":"38"},{"id":"Nicaragua","color":"rgb(102,153,255)","label":"Nicaragua","y":"41","size":2,"x":"56"},{"id":"Bolivia","color":"rgb(102,153,255)","label":"Bolivia","y":"20","size":2,"x":"24"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":2}]',
        'cities': '{"features":[{"city":{"title":"3 Latin American leaders offer Snowden asylum","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/07/content_16743494.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Latin America, Venezuela, Edward Snowden, Nicaragua, Bolivia'

        },
        {'start': 'Jul 07 2013 11:50:00 GMT-0100', 
        'title': 'Bolivia willing to offer asylum to Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":55,"edges":[{"id":"Evo MoralesLa Paz","weight":20,"source":"Evo Morales","target":"La Paz","label":""},{"id":"Evo MoralesEdward Snowden","weight":20,"source":"Evo Morales","target":"Edward Snowden","label":""},{"id":"Evo MoralesBolivia","weight":20,"source":"Evo Morales","target":"Bolivia","label":""},{"id":"La PazEdward Snowden","weight":20,"source":"La Paz","target":"Edward Snowden","label":""},{"id":"La PazBolivia","weight":20,"source":"La Paz","target":"Bolivia","label":""},{"id":"Edward SnowdenBolivia","weight":20,"source":"Edward Snowden","target":"Bolivia","label":""}],"nodes":[{"id":"Evo Morales","color":"rgb(102,153,255)","label":"Evo Morales","y":"53","size":2,"x":"72"},{"id":"La Paz","color":"rgb(102,153,255)","label":"La Paz","y":"22","size":2,"x":"15"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"51","size":2,"x":"18"},{"id":"Bolivia","color":"rgb(102,153,255)","label":"Bolivia","y":"91","size":2,"x":"45"}]}',
        'emo': '[{"term":"asylum","value":2}]',
        'cities': '{"features":[{"city":{"title":"Bolivia willing to offer asylum to Snowden","lon":-101.74610137939453,"name":"La Paz","lat":20.22640037536621}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/07/content_16742819.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , La Paz, Evo Morales, Edward Snowden, Bolivia'

        },
        {'start': 'Jul 05 2013 08:16:00 GMT-0100', 
        'title': 'Russia more impatient over Snowdens stay',
        'classname': 'chinadaily',
        'graph': '{"id":56,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"8","size":2,"x":"88"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"22","size":2,"x":"4"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"3","size":2,"x":"39"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Russia more impatient over Snowdens stay","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/05/content_16731654.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jul 05 2013 01:18:00 GMT-0100', 
        'title': 'EU and US set for free trade negotiations',
        'classname': 'chinadaily',
        'graph': '{"id":57,"edges":[{"id":"European ParliamentEU","weight":20,"source":"European Parliament","target":"EU","label":""}],"nodes":[{"id":"European Parliament","color":"rgb(102,153,255)","label":"European Parliament","y":"19","size":2,"x":"82"},{"id":"EU","color":"rgb(102,153,255)","label":"EU","y":"32","size":2,"x":"94"}]}',
        'emo': '[{"term":"freedom","value":2},{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"EU and US set for free trade negotiations","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/05/content_16730854.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , European Parliament, EU'

        },
        {'start': 'Jul 04 2013 08:00:00 GMT-0100', 
        'title': 'Obama, Merkel agree talks on surveillance program',
        'classname': 'chinadaily',
        'graph': '{"id":58,"edges":[{"id":"Barack ObamaWhite House","weight":20,"source":"Barack Obama","target":"White House","label":""},{"id":"Barack ObamaGermany","weight":20,"source":"Barack Obama","target":"Germany","label":""},{"id":"Barack ObamaAngela Merkel","weight":20,"source":"Barack Obama","target":"Angela Merkel","label":""},{"id":"White HouseGermany","weight":20,"source":"White House","target":"Germany","label":""},{"id":"White HouseAngela Merkel","weight":20,"source":"White House","target":"Angela Merkel","label":""},{"id":"GermanyAngela Merkel","weight":20,"source":"Germany","target":"Angela Merkel","label":""}],"nodes":[{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"80","size":2,"x":"45"},{"id":"White House","color":"rgb(102,153,255)","label":"White House","y":"51","size":2,"x":"40"},{"id":"Germany","color":"rgb(102,153,255)","label":"Germany","y":"20","size":2,"x":"52"},{"id":"Angela Merkel","color":"rgb(102,153,255)","label":"Angela Merkel","y":"8","size":2,"x":"93"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/04/content_16722976.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Barack Obama, White House, Germany, Angela Merkel'

        },
        {'start': 'Jul 04 2013 07:14:00 GMT-0100', 
        'title': 'Snowden still in Moscow despite Bolivian plane drama',
        'classname': 'chinadaily',
        'graph': '{"id":59,"edges":[{"id":"United StatesEvo Morales","weight":20,"source":"United States","target":"Evo Morales","label":""},{"id":"United StatesLatin America","weight":20,"source":"United States","target":"Latin America","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesBolivia","weight":20,"source":"United States","target":"Bolivia","label":""},{"id":"Evo MoralesLatin America","weight":20,"source":"Evo Morales","target":"Latin America","label":""},{"id":"Evo MoralesEdward Snowden","weight":20,"source":"Evo Morales","target":"Edward Snowden","label":""},{"id":"Evo MoralesBolivia","weight":20,"source":"Evo Morales","target":"Bolivia","label":""},{"id":"Latin AmericaEdward Snowden","weight":20,"source":"Latin America","target":"Edward Snowden","label":""},{"id":"Latin AmericaBolivia","weight":20,"source":"Latin America","target":"Bolivia","label":""},{"id":"Edward SnowdenBolivia","weight":20,"source":"Edward Snowden","target":"Bolivia","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"80","size":2,"x":"62"},{"id":"Evo Morales","color":"rgb(102,153,255)","label":"Evo Morales","y":"98","size":2,"x":"8"},{"id":"Latin America","color":"rgb(102,153,255)","label":"Latin America","y":"43","size":2,"x":"87"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"52","size":2,"x":"85"},{"id":"Bolivia","color":"rgb(102,153,255)","label":"Bolivia","y":"4","size":2,"x":"5"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden still in Moscow despite Bolivian plane drama","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/04/content_16722495.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Evo Morales, Moscow, Latin America, Edward Snowden, Bolivia'

        },
        {'start': 'Jul 03 2013 07:14:00 GMT-0100', 
        'title': 'Bolivian presidents plane leaves Vienna',
        'classname': 'chinadaily',
        'graph': '{"id":60,"edges":[{"id":"Evo MoralesEdward Snowden","weight":20,"source":"Evo Morales","target":"Edward Snowden","label":""},{"id":"Evo MoralesBolivia","weight":20,"source":"Evo Morales","target":"Bolivia","label":""},{"id":"Edward SnowdenBolivia","weight":20,"source":"Edward Snowden","target":"Bolivia","label":""}],"nodes":[{"id":"Evo Morales","color":"rgb(102,153,255)","label":"Evo Morales","y":"56","size":2,"x":"20"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"5","size":2,"x":"91"},{"id":"Bolivia","color":"rgb(102,153,255)","label":"Bolivia","y":"80","size":2,"x":"9"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/03/content_16720134.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Evo Morales, Edward Snowden, Vienna, Bolivia'

        },
        {'start': 'Jul 02 2013 08:08:00 GMT-0100', 
        'title': 'India turns down Snowdens asylum request',
        'classname': 'chinadaily',
        'graph': '{"id":61,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowIndia","weight":20,"source":"Moscow","target":"India","label":""},{"id":"Edward SnowdenIndia","weight":20,"source":"Edward Snowden","target":"India","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"77","size":2,"x":"60"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"22","size":2,"x":"13"},{"id":"India","color":"rgb(102,153,255)","label":"India","y":"15","size":2,"x":"72"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"India turns down Snowdens asylum request","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/02/content_16710277.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, India'

        },
        {'start': 'Jul 02 2013 04:55:00 GMT-0100', 
        'title': 'Snowden drops Russia asylum request: Kremlin',
        'classname': 'chinadaily',
        'graph': '{"id":62,"edges":[{"id":"KremlinEdward Snowden","weight":20,"source":"Kremlin","target":"Edward Snowden","label":""},{"id":"KremlinRussia","weight":20,"source":"Kremlin","target":"Russia","label":""},{"id":"KremlinVladimir Putin","weight":20,"source":"Kremlin","target":"Vladimir Putin","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVladimir Putin","weight":20,"source":"Edward Snowden","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Kremlin","color":"rgb(102,153,255)","label":"Kremlin","y":"46","size":2,"x":"9"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"18","size":2,"x":"23"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"93","size":2,"x":"5"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"75","size":2,"x":"12"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/02/content_16709035.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Kremlin, Edward Snowden, Russia, Vladimir Putin'

        },
        {'start': 'Jul 02 2013 04:23:00 GMT-0100', 
        'title': 'Snowden formally asks political asylum in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":63,"edges":[{"id":"Foreign MinistryEdward Snowden","weight":20,"source":"Foreign Ministry","target":"Edward Snowden","label":""},{"id":"Foreign MinistryRussia","weight":20,"source":"Foreign Ministry","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Foreign Ministry","color":"rgb(102,153,255)","label":"Foreign Ministry","y":"17","size":2,"x":"43"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"77","size":2,"x":"96"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"97","size":2,"x":"81"}]}',
        'emo': '[{"term":"asylum","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/02/content_16701834.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Foreign Ministry, Kremlin, Edward Snowden, Russia'

        },
        {'start': 'Jul 02 2013 04:55:00 GMT-0100', 
        'title': 'Norway to reject Snowdens application for asylum',
        'classname': 'chinadaily',
        'graph': '{"id":64,"edges":[],"nodes":[]}',
        'emo': '[{"term":"asylum","value":2},{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/02/content_16709036.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Jul 02 2013 08:43:00 GMT-0100', 
        'title': 'Immature US diplomacy',
        'classname': 'chinadaily',
        'graph': '{"id":65,"edges":[{"id":"Hong KongWhite House","weight":20,"source":"Hong Kong","target":"White House","label":""},{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""},{"id":"Hong KongCIA","weight":20,"source":"Hong Kong","target":"CIA","label":""},{"id":"White HouseEdward Snowden","weight":20,"source":"White House","target":"Edward Snowden","label":""},{"id":"White HouseCIA","weight":20,"source":"White House","target":"CIA","label":""},{"id":"Edward SnowdenCIA","weight":20,"source":"Edward Snowden","target":"CIA","label":""},{"id":"Hong KongMoscow","weight":20,"source":"Hong Kong","target":"Moscow","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"2","size":4,"x":"83"},{"id":"White House","color":"rgb(102,153,255)","label":"White House","y":"80","size":2,"x":"48"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"10","size":2,"x":"69"},{"id":"CIA","color":"rgb(102,153,255)","label":"CIA","y":"91","size":2,"x":"89"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"43","size":2,"x":"11"}]}',
        'emo': '[{"term":"hacker","value":1}]',
        'cities': '{"features":[{"city":{"title":"Immature US diplomacy","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}},{"city":{"title":"Immature US diplomacy","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/02/content_16703002.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, Moscow, White House, CIA, Edward Snowden, China'

        },
        {'start': 'Jul 02 2013 03:27:00 GMT-0100', 
        'title': 'Interview with former FBI investigator Robert King Wittman',
        'classname': 'chinadaily',
        'graph': '{"id":66,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-07/02/content_16701638.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , FBI'

        },
        {'start': 'Jul 02 2013 11:09:00 GMT-0100', 
        'title': 'Q & A with the investigator',
        'classname': 'chinadaily',
        'graph': '{"id":67,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/culture/2013-07/02/content_16705177.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 01 2013 07:42:00 GMT-0100', 
        'title': 'Snowden case vindicates need for Web security rule',
        'classname': 'chinadaily',
        'graph': '{"id":68,"edges":[{"id":"United StatesEU","weight":20,"source":"United States","target":"EU","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"EUEdward Snowden","weight":20,"source":"EU","target":"Edward Snowden","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"21","size":2,"x":"25"},{"id":"EU","color":"rgb(102,153,255)","label":"EU","y":"17","size":2,"x":"72"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"41","size":2,"x":"11"}]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"press","value":1},{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/01/content_16700427.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Edward Snowden, EU'

        },
        {'start': 'Jun 30 2013 08:13:00 GMT-0100', 
        'title': 'EU questions US over alleged spying: media',
        'classname': 'chinadaily',
        'graph': '{"id":69,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/30/content_16691974.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , EU'

        },
        {'start': 'Jun 29 2013 07:37:00 GMT-0100', 
        'title': 'West must work with the rest to secure Internet',
        'classname': 'chinadaily',
        'graph': '{"id":70,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/29/content_16686275.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Internet'

        },
        {'start': 'Jun 28 2013 10:26:00 GMT-0100', 
        'title': 'Summit sets new course for future',
        'classname': 'chinadaily',
        'graph': '{"id":71,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/epaper/2013-06/28/content_16679617.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 28 2013 10:18:00 GMT-0100', 
        'title': 'IN BRIEF (Page 2)',
        'classname': 'chinadaily',
        'graph': '{"id":72,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/epaper/2013-06/28/content_16679362.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 28 2013 10:20:00 GMT-0100', 
        'title': 'Ecuador defends decision to consider Snowdens asylum bid',
        'classname': 'chinadaily',
        'graph': '{"id":73,"edges":[{"id":"EcuadorEdward Snowden","weight":20,"source":"Ecuador","target":"Edward Snowden","label":""}],"nodes":[{"id":"Ecuador","color":"rgb(102,153,255)","label":"Ecuador","y":"56","size":2,"x":"40"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"49","size":2,"x":"95"}]}',
        'emo': '[{"term":"asylum","value":2}]',
        'cities': '{"features":[{"city":{"title":"Ecuador defends decision to consider Snowdens asylum bid","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/28/content_16679504.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Ecuador, Edward Snowden'

        },
        {'start': 'Jun 28 2013 10:02:00 GMT-0100', 
        'title': 'Snowden could request asylum in Russia: official',
        'classname': 'chinadaily',
        'graph': '{"id":74,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"79","size":2,"x":"78"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"3","size":2,"x":"90"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"43","size":2,"x":"82"}]}',
        'emo': '[{"term":"asylum","value":2}]',
        'cities': '{"features":[{"city":{"title":"Snowden could request asylum in Russia: official","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/28/content_16679185.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jun 27 2013 06:36:00 GMT-0100', 
        'title': 'Snowden still at Moscows airport, asylum pending',
        'classname': 'chinadaily',
        'graph': '{"id":75,"edges":[],"nodes":[]}',
        'emo': '[{"term":"asylum","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden still at Moscows airport, asylum pending","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/27/content_16667935.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow'

        },
        {'start': 'Jun 27 2013 05:48:00 GMT-0100', 
        'title': 'China can curb credit crunch: ADB official',
        'classname': 'chinadaily',
        'graph': '{"id":76,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-06/27/content_16668916.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , China'

        },
        {'start': 'Jun 25 2013 06:48:00 GMT-0100', 
        'title': 'US accusations over Snowden groundless: Russian FM',
        'classname': 'chinadaily',
        'graph': '{"id":77,"edges":[{"id":"Sergei LavrovEdward Snowden","weight":20,"source":"Sergei Lavrov","target":"Edward Snowden","label":""},{"id":"Sergei LavrovRussia","weight":20,"source":"Sergei Lavrov","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Sergei Lavrov","color":"rgb(102,153,255)","label":"Sergei Lavrov","y":"77","size":2,"x":"31"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"58","size":2,"x":"77"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"5","size":2,"x":"6"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/25/content_16657880.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Sergei Lavrov, Edward Snowden, Russia'

        },
        {'start': 'Jun 25 2013 09:34:00 GMT-0100', 
        'title': 'China rejects US accusation on Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":78,"edges":[{"id":"United StatesHong Kong","weight":20,"source":"United States","target":"Hong Kong","label":""},{"id":"United StatesForeign Ministry","weight":20,"source":"United States","target":"Foreign Ministry","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesChina","weight":20,"source":"United States","target":"China","label":""},{"id":"Hong KongForeign Ministry","weight":20,"source":"Hong Kong","target":"Foreign Ministry","label":""},{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""},{"id":"Hong KongChina","weight":20,"source":"Hong Kong","target":"China","label":""},{"id":"Foreign MinistryEdward Snowden","weight":20,"source":"Foreign Ministry","target":"Edward Snowden","label":""},{"id":"Foreign MinistryChina","weight":20,"source":"Foreign Ministry","target":"China","label":""},{"id":"Edward SnowdenChina","weight":20,"source":"Edward Snowden","target":"China","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"7","size":2,"x":"97"},{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"87","size":2,"x":"23"},{"id":"Foreign Ministry","color":"rgb(102,153,255)","label":"Foreign Ministry","y":"73","size":2,"x":"52"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"9","size":2,"x":"85"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"94","size":2,"x":"40"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"China rejects US accusation on Snowden","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/25/content_16658276.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Hong Kong, Foreign Ministry, Edward Snowden, China'

        },
        {'start': 'Jun 24 2013 09:21:00 GMT-0100', 
        'title': 'Ecuador reviewing Snowdens asylum request',
        'classname': 'chinadaily',
        'graph': '{"id":79,"edges":[{"id":"EcuadorEdward Snowden","weight":20,"source":"Ecuador","target":"Edward Snowden","label":""},{"id":"EcuadorRussia","weight":20,"source":"Ecuador","target":"Russia","label":""},{"id":"EcuadorVietnam","weight":20,"source":"Ecuador","target":"Vietnam","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVietnam","weight":20,"source":"Edward Snowden","target":"Vietnam","label":""},{"id":"RussiaVietnam","weight":20,"source":"Russia","target":"Vietnam","label":""}],"nodes":[{"id":"Ecuador","color":"rgb(102,153,255)","label":"Ecuador","y":"11","size":2,"x":"69"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"90","size":2,"x":"98"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"72","size":2,"x":"64"},{"id":"Vietnam","color":"rgb(102,153,255)","label":"Vietnam","y":"40","size":2,"x":"53"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"press","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/24/content_16653311.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Ecuador, Edward Snowden, Russia, Vietnam'

        },
        {'start': 'Jun 24 2013 07:56:00 GMT-0100', 
        'title': 'Moscow-Havana flight departs without Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":80,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"10","size":2,"x":"37"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"36","size":2,"x":"57"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Moscow-Havana flight departs without Snowden","lon":-82.36419677734375,"name":"Havana","lat":23.131900787353516}},{"city":{"title":"Moscow-Havana flight departs without Snowden","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/24/content_16653130.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden'

        },
        {'start': 'Jun 23 2013 04:32:00 GMT-0100', 
        'title': 'Snowden leaves Hong Kong: govt',
        'classname': 'chinadaily',
        'graph': '{"id":81,"edges":[{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"92","size":4,"x":"32"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"22","size":2,"x":"1"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden leaves Hong Kong: govt","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/23/content_16647856.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, Edward Snowden'

        },
        {'start': 'Jun 20 2013 07:56:00 GMT-0100', 
        'title': 'Developed countries keen to show united front',
        'classname': 'chinadaily',
        'graph': '{"id":82,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/20/content_16639031.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Russia'

        },
        {'start': 'Jun 19 2013 09:51:00 GMT-0100', 
        'title': 'Exceptionalism harms the US and the world',
        'classname': 'chinadaily',
        'graph': '{"id":83,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/19/content_16638619.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 18 2013 12:55:00 GMT-0100', 
        'title': 'Snowden denies being Chinese spy: media',
        'classname': 'chinadaily',
        'graph': '{"id":84,"edges":[{"id":"Edward SnowdenChina","weight":20,"source":"Edward Snowden","target":"China","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"3","size":2,"x":"66"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"34","size":2,"x":"89"}]}',
        'emo': '[{"term":"surveillance","value":2},{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/18/content_16633111.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, China'

        },
        {'start': 'Jun 17 2013 11:33:00 GMT-0100', 
        'title': 'China rejects spy claims against Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":85,"edges":[{"id":"Hong KongChina","weight":20,"source":"Hong Kong","target":"China","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"32","size":2,"x":"90"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"4","size":2,"x":"50"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"China rejects spy claims against Snowden","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/17/content_16631643.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, China'

        },
        {'start': 'Aug 22 2013 06:38:00 GMT-0100', 
        'title': 'Manning gets 35 years in WikiLeaks trial',
        'classname': 'chinadaily',
        'graph': '{"id":86,"edges":[],"nodes":[]}',
        'emo': '[{"term":"jail","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/22/content_16912402.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Bradley Manning'

        },
        {'start': 'Aug 24 2013 05:04:00 GMT-0100', 
        'title': 'Trending news across China',
        'classname': 'chinadaily',
        'graph': '{"id":87,"edges":[],"nodes":[]}',
        'emo': '[{"term":"public","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-08/24/content_16918531.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , China'

        },
        {'start': 'Aug 22 2013 09:11:00 GMT-0100', 
        'title': 'WikiLeaker Manning says is female, wants to live as a woman',
        'classname': 'chinadaily',
        'graph': '{"id":88,"edges":[],"nodes":[]}',
        'emo': '[{"term":"jail","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/22/content_16914647.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Bradley Manning'

        },
        {'start': 'Aug 21 2013 08:04:00 GMT-0100', 
        'title': 'Manning to be sentenced on Wed',
        'classname': 'chinadaily',
        'graph': '{"id":89,"edges":[{"id":"US ArmyBradley Manning","weight":20,"source":"US Army","target":"Bradley Manning","label":""}],"nodes":[{"id":"US Army","color":"rgb(102,153,255)","label":"US Army","y":"86","size":2,"x":"44"},{"id":"Bradley Manning","color":"rgb(102,153,255)","label":"Bradley Manning","y":"76","size":2,"x":"40"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/21/content_16909415.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , US Army, Bradley Manning'

        },
        {'start': 'Aug 12 2013 07:12:00 GMT-0100', 
        'title': 'Assange lauds surveillance reversal',
        'classname': 'chinadaily',
        'graph': '{"id":90,"edges":[{"id":"Barack ObamaEdward Snowden","weight":20,"source":"Barack Obama","target":"Edward Snowden","label":""},{"id":"Barack ObamaJulian Assange","weight":20,"source":"Barack Obama","target":"Julian Assange","label":""},{"id":"Edward SnowdenJulian Assange","weight":20,"source":"Edward Snowden","target":"Julian Assange","label":""}],"nodes":[{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"38","size":2,"x":"45"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"30","size":2,"x":"50"},{"id":"Julian Assange","color":"rgb(102,153,255)","label":"Julian Assange","y":"62","size":2,"x":"47"}]}',
        'emo': '[{"term":"surveillance","value":3}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/12/content_16886349.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Barack Obama, Edward Snowden, Julian Assange'

        },
        {'start': 'Aug 15 2013 06:43:00 GMT-0100', 
        'title': 'Manning sorry for US secrets breach',
        'classname': 'chinadaily',
        'graph': '{"id":91,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/15/content_16894831.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Bradley Manning'

        },
        {'start': 'Aug 02 2013 07:08:00 GMT-0100', 
        'title': 'US crusade to hide the truth of spying',
        'classname': 'chinadaily',
        'graph': '{"id":92,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-08/02/content_16886902.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Aug 02 2013 10:10:00 GMT-0100', 
        'title': 'Number of leaked documents was overwhelming',
        'classname': 'chinadaily',
        'graph': '{"id":93,"edges":[{"id":"US ArmyBradley Manning","weight":20,"source":"US Army","target":"Bradley Manning","label":""}],"nodes":[{"id":"US Army","color":"rgb(102,153,255)","label":"US Army","y":"86","size":2,"x":"68"},{"id":"Bradley Manning","color":"rgb(102,153,255)","label":"Bradley Manning","y":"17","size":2,"x":"7"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/02/content_16865196.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , US Army, Bradley Manning'

        },
        {'start': 'Aug 01 2013 08:35:00 GMT-0100', 
        'title': 'Snowden has entered Russia, lawyer says',
        'classname': 'chinadaily',
        'graph': '{"id":94,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"18","size":2,"x":"62"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"46","size":2,"x":"38"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"81","size":2,"x":"43"}]}',
        'emo': '[{"term":"lawyer","value":2}]',
        'cities': '{"features":[{"city":{"title":"Snowden has entered Russia, lawyer says","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/01/content_16863481.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Aug 02 2013 07:08:00 GMT-0100', 
        'title': 'US crusade to hide the truth of spying',
        'classname': 'chinadaily',
        'graph': '{"id":95,"edges":[{"id":"NSAUnited States","weight":20,"source":"NSA","target":"United States","label":""},{"id":"NSABradley Manning","weight":20,"source":"NSA","target":"Bradley Manning","label":""},{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""},{"id":"NSAJulian Assange","weight":20,"source":"NSA","target":"Julian Assange","label":""},{"id":"United StatesBradley Manning","weight":20,"source":"United States","target":"Bradley Manning","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesJulian Assange","weight":20,"source":"United States","target":"Julian Assange","label":""},{"id":"Bradley ManningEdward Snowden","weight":20,"source":"Bradley Manning","target":"Edward Snowden","label":""},{"id":"Bradley ManningJulian Assange","weight":20,"source":"Bradley Manning","target":"Julian Assange","label":""},{"id":"Edward SnowdenJulian Assange","weight":20,"source":"Edward Snowden","target":"Julian Assange","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"76","size":2,"x":"33"},{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"84","size":2,"x":"35"},{"id":"Bradley Manning","color":"rgb(102,153,255)","label":"Bradley Manning","y":"12","size":2,"x":"4"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"66","size":2,"x":"19"},{"id":"Julian Assange","color":"rgb(102,153,255)","label":"Julian Assange","y":"4","size":2,"x":"31"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-08/02/content_16864497.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, NSA, , Bradley Manning, Edward Snowden, Julian Assange'

        },
        {'start': 'Jul 31 2013 07:52:00 GMT-0100', 
        'title': 'Manning guilty of 20 charges, not aiding the enemy',
        'classname': 'chinadaily',
        'graph': '{"id":96,"edges":[],"nodes":[]}',
        'emo': '[{"term":"jail","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/31/content_16854894.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , US Army, Bradley Manning'

        },
        {'start': 'Jul 13 2013 10:09:00 GMT-0100', 
        'title': 'Obama speaks with Putin on Snowden, but no movement',
        'classname': 'chinadaily',
        'graph': '{"id":97,"edges":[{"id":"MoscowBarack Obama","weight":20,"source":"Moscow","target":"Barack Obama","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"MoscowVladimir Putin","weight":20,"source":"Moscow","target":"Vladimir Putin","label":""},{"id":"Barack ObamaEdward Snowden","weight":20,"source":"Barack Obama","target":"Edward Snowden","label":""},{"id":"Barack ObamaRussia","weight":20,"source":"Barack Obama","target":"Russia","label":""},{"id":"Barack ObamaVladimir Putin","weight":20,"source":"Barack Obama","target":"Vladimir Putin","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVladimir Putin","weight":20,"source":"Edward Snowden","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"69","size":2,"x":"23"},{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"21","size":2,"x":"28"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"18","size":2,"x":"70"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"27","size":2,"x":"25"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"46","size":2,"x":"93"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Obama speaks with Putin on Snowden, but no movement","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}},{"city":{"title":"Obama speaks with Putin on Snowden, but no movement","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/13/content_16770896.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Barack Obama, Edward Snowden, Russia, Vladimir Putin'

        },
        {'start': 'Jul 17 2013 09:35:00 GMT-0100', 
        'title': 'Russia has no legal grounds to extradite Snowden: lawyer',
        'classname': 'chinadaily',
        'graph': '{"id":98,"edges":[],"nodes":[]}',
        'emo': '[{"term":"legal","value":1},{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/17/content_16790778.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Russia'

        },
        {'start': 'Jul 13 2013 01:40:00 GMT-0100', 
        'title': 'Snowden seeks political asylum in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":99,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"38","size":2,"x":"93"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"26","size":2,"x":"3"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":1},{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/13/content_16770114.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 11 2013 10:35:00 GMT-0100', 
        'title': 'Snowden is likely Venezuela bound',
        'classname': 'chinadaily',
        'graph': '{"id":100,"edges":[{"id":"United StatesVenezuela","weight":20,"source":"United States","target":"Venezuela","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesGlenn Greenwald","weight":20,"source":"United States","target":"Glenn Greenwald","label":""},{"id":"VenezuelaEdward Snowden","weight":20,"source":"Venezuela","target":"Edward Snowden","label":""},{"id":"VenezuelaGlenn Greenwald","weight":20,"source":"Venezuela","target":"Glenn Greenwald","label":""},{"id":"Edward SnowdenGlenn Greenwald","weight":20,"source":"Edward Snowden","target":"Glenn Greenwald","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"4","size":2,"x":"52"},{"id":"Venezuela","color":"rgb(102,153,255)","label":"Venezuela","y":"97","size":2,"x":"51"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"57","size":2,"x":"73"},{"id":"Glenn Greenwald","color":"rgb(102,153,255)","label":"Glenn Greenwald","y":"55","size":2,"x":"59"}]}',
        'emo': '[{"term":"asylum","value":1},{"term":"escape","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/11/content_16761143.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Venezuela, Edward Snowden, Glenn Greenwald'

        },
        {'start': 'Jul 10 2013 06:47:00 GMT-0100', 
        'title': 'Snowden has not accepted asylum in Venezuela: WikiLeaks',
        'classname': 'chinadaily',
        'graph': '{"id":101,"edges":[{"id":"VenezuelaEdward Snowden","weight":20,"source":"Venezuela","target":"Edward Snowden","label":""},{"id":"VenezuelaRussia","weight":20,"source":"Venezuela","target":"Russia","label":""},{"id":"VenezuelaTwitter","weight":20,"source":"Venezuela","target":"Twitter","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenTwitter","weight":20,"source":"Edward Snowden","target":"Twitter","label":""},{"id":"RussiaTwitter","weight":20,"source":"Russia","target":"Twitter","label":""}],"nodes":[{"id":"Venezuela","color":"rgb(102,153,255)","label":"Venezuela","y":"96","size":2,"x":"23"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"14","size":2,"x":"81"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"62","size":2,"x":"74"},{"id":"Twitter","color":"rgb(102,153,255)","label":"Twitter","y":"13","size":2,"x":"39"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/10/content_16754475.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Venezuela, Edward Snowden, Russia, Twitter'

        },
        {'start': 'Jul 04 2013 11:50:00 GMT-0100', 
        'title': 'Russia not to decide Snowdens fate: diplomat',
        'classname': 'chinadaily',
        'graph': '{"id":102,"edges":[{"id":"NSAMoscow","weight":20,"source":"NSA","target":"Moscow","label":""},{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""},{"id":"NSARussia","weight":20,"source":"NSA","target":"Russia","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"37","size":2,"x":"38"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"91","size":2,"x":"80"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"72","size":2,"x":"7"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"93","size":2,"x":"9"}]}',
        'emo': '[{"term":"asylum","value":1}]',
        'cities': '{"features":[{"city":{"title":"Russia not to decide Snowdens fate: diplomat","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/04/content_16730615.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jul 03 2013 10:30:00 GMT-0100', 
        'title': 'Bolivia denies Snowden on presidents plane',
        'classname': 'chinadaily',
        'graph': '{"id":103,"edges":[{"id":"Edward SnowdenBolivia","weight":20,"source":"Edward Snowden","target":"Bolivia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"85","size":2,"x":"51"},{"id":"Bolivia","color":"rgb(102,153,255)","label":"Bolivia","y":"52","size":2,"x":"54"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/03/content_16714983.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Bolivia'

        },
        {'start': 'Jul 04 2013 10:14:00 GMT-0100', 
        'title': 'Ecuador finds spy mic for Assange meeting',
        'classname': 'chinadaily',
        'graph': '{"id":104,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/04/content_16725116.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Ecuador'

        },
        {'start': 'Jul 03 2013 07:48:00 GMT-0100', 
        'title': 'Prosecution wraps up Wikileaks case in court-martial',
        'classname': 'chinadaily',
        'graph': '{"id":105,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/03/content_16712278.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Wikileaks'

        },
        {'start': 'Jul 03 2013 01:53:00 GMT-0100', 
        'title': 'Global response to Snowdens asylum applications',
        'classname': 'chinadaily',
        'graph': '{"id":106,"edges":[],"nodes":[]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"asylum","value":2},{"term":"surveillance","value":2}]',
        'cities': '{"features":[{"city":{"title":"Global response to Snowdens asylum applications","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/03/content_16716835.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Jul 02 2013 08:09:00 GMT-0100', 
        'title': 'Hollande urges common European stance on US spying',
        'classname': 'chinadaily',
        'graph': '{"id":107,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/02/content_16710280.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Francois Hollande'

        },
        {'start': 'Jul 02 2013 04:56:00 GMT-0100', 
        'title': 'Snowden deserves worlds protection: Venezuela',
        'classname': 'chinadaily',
        'graph': '{"id":108,"edges":[{"id":"VenezuelaEdward Snowden","weight":20,"source":"Venezuela","target":"Edward Snowden","label":""},{"id":"VenezuelaNicolas Maduro","weight":20,"source":"Venezuela","target":"Nicolas Maduro","label":""},{"id":"VenezuelaCaracas","weight":20,"source":"Venezuela","target":"Caracas","label":""},{"id":"Edward SnowdenNicolas Maduro","weight":20,"source":"Edward Snowden","target":"Nicolas Maduro","label":""},{"id":"Edward SnowdenCaracas","weight":20,"source":"Edward Snowden","target":"Caracas","label":""},{"id":"Nicolas MaduroCaracas","weight":20,"source":"Nicolas Maduro","target":"Caracas","label":""}],"nodes":[{"id":"Venezuela","color":"rgb(102,153,255)","label":"Venezuela","y":"71","size":2,"x":"1"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"11","size":2,"x":"7"},{"id":"Nicolas Maduro","color":"rgb(102,153,255)","label":"Nicolas Maduro","y":"55","size":2,"x":"57"},{"id":"Caracas","color":"rgb(102,153,255)","label":"Caracas","y":"1","size":2,"x":"89"}]}',
        'emo': '[{"term":"asylum","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden deserves worlds protection: Venezuela","lon":-80.41670227050781,"name":"Caracas","lat":-0.6000000238418579}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/02/content_16709044.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Venezuela, Caracas, Nicolas Maduro, Edward Snowden'

        },
        {'start': 'Jul 01 2013 06:30:00 GMT-0100', 
        'title': 'Snowden info will keep getting out',
        'classname': 'chinadaily',
        'graph': '{"id":109,"edges":[{"id":"Edward SnowdenJulian Assange","weight":20,"source":"Edward Snowden","target":"Julian Assange","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"55","size":2,"x":"9"},{"id":"Julian Assange","color":"rgb(102,153,255)","label":"Julian Assange","y":"33","size":2,"x":"61"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/01/content_16692916.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Julian Assange'

        },
        {'start': 'Jun 29 2013 09:19:00 GMT-0100', 
        'title': 'Russia says its in tough spot over Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":110,"edges":[{"id":"Hong KongMoscow","weight":20,"source":"Hong Kong","target":"Moscow","label":""},{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""},{"id":"Hong KongRussia","weight":20,"source":"Hong Kong","target":"Russia","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"11","size":2,"x":"5"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"35","size":2,"x":"51"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"25","size":2,"x":"44"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"56","size":2,"x":"24"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Russia says its in tough spot over Snowden","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}},{"city":{"title":"Russia says its in tough spot over Snowden","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}},{"city":{"title":"Russia says its in tough spot over Snowden","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/29/content_16686579.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jun 27 2013 05:03:00 GMT-0100', 
        'title': 'More Americans see Snowden as patriot: Poll',
        'classname': 'chinadaily',
        'graph': '{"id":111,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2},{"term":"public","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/27/content_16674245.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Jun 26 2013 06:34:00 GMT-0100', 
        'title': 'Russia rules out Snowden expulsion',
        'classname': 'chinadaily',
        'graph': '{"id":112,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/26/content_16659035.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Russia'

        },
        {'start': 'Jun 26 2013 06:51:00 GMT-0100', 
        'title': 'US explanation needed',
        'classname': 'chinadaily',
        'graph': '{"id":113,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/26/content_16659281.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 25 2013 06:36:00 GMT-0100', 
        'title': 'US presses Russia as mystery over Snowden deepens',
        'classname': 'chinadaily',
        'graph': '{"id":114,"edges":[{"id":"United StatesMoscow","weight":20,"source":"United States","target":"Moscow","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"54","size":2,"x":"98"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"84","size":2,"x":"71"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"56","size":2,"x":"91"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"78","size":2,"x":"97"}]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"US presses Russia as mystery over Snowden deepens","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/25/content_16654019.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jun 24 2013 11:23:00 GMT-0100', 
        'title': 'Snowden exposes more US hacking, then flies',
        'classname': 'chinadaily',
        'graph': '{"id":115,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/24/content_16651881.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 24 2013 02:40:00 GMT-0100', 
        'title': 'Snowdens destination unknown',
        'classname': 'chinadaily',
        'graph': '{"id":116,"edges":[{"id":"Hong KongMoscow","weight":20,"source":"Hong Kong","target":"Moscow","label":""},{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"80","size":2,"x":"68"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"52","size":2,"x":"11"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"78","size":2,"x":"19"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Snowdens destination unknown","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}},{"city":{"title":"Snowdens destination unknown","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}},{"city":{"title":"Snowdens destination unknown","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/24/content_16648409.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, Moscow, Edward Snowden'

        },
        {'start': 'Jun 24 2013 07:24:00 GMT-0100', 
        'title': 'Snowden in Russia seeking Ecuador asylum',
        'classname': 'chinadaily',
        'graph': '{"id":117,"edges":[],"nodes":[]}',
        'emo': '[{"term":"asylum","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/24/content_16648761.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Ecuador, Russia'

        },
        {'start': 'Jun 22 2013 08:47:00 GMT-0100', 
        'title': 'US files criminal charges against Snowden over leaks',
        'classname': 'chinadaily',
        'graph': '{"id":118,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/22/content_16645986.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Jun 21 2013 09:40:00 GMT-0100', 
        'title': 'Private jet ready to take Snowden to Iceland',
        'classname': 'chinadaily',
        'graph': '{"id":119,"edges":[],"nodes":[]}',
        'emo': '[{"term":"privacy","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/21/content_16642928.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 19 2013 07:24:00 GMT-0100', 
        'title': 'Snowden approaches Iceland for asylum',
        'classname': 'chinadaily',
        'graph': '{"id":120,"edges":[],"nodes":[]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/19/content_16635568.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Jun 18 2013 08:12:00 GMT-0100', 
        'title': 'Ecuador may offer whistleblower asylum',
        'classname': 'chinadaily',
        'graph': '{"id":121,"edges":[{"id":"NSAEcuador","weight":20,"source":"NSA","target":"Ecuador","label":""},{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""},{"id":"NSACIA","weight":20,"source":"NSA","target":"CIA","label":""},{"id":"NSAPRISM","weight":20,"source":"NSA","target":"PRISM","label":""},{"id":"EcuadorEdward Snowden","weight":20,"source":"Ecuador","target":"Edward Snowden","label":""},{"id":"EcuadorCIA","weight":20,"source":"Ecuador","target":"CIA","label":""},{"id":"EcuadorPRISM","weight":20,"source":"Ecuador","target":"PRISM","label":""},{"id":"Edward SnowdenCIA","weight":20,"source":"Edward Snowden","target":"CIA","label":""},{"id":"Edward SnowdenPRISM","weight":20,"source":"Edward Snowden","target":"PRISM","label":""},{"id":"CIAPRISM","weight":20,"source":"CIA","target":"PRISM","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"25","size":2,"x":"4"},{"id":"Ecuador","color":"rgb(102,153,255)","label":"Ecuador","y":"29","size":2,"x":"82"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"15","size":2,"x":"37"},{"id":"CIA","color":"rgb(102,153,255)","label":"CIA","y":"79","size":2,"x":"61"},{"id":"PRISM","color":"rgb(102,153,255)","label":"PRISM","y":"26","size":2,"x":"23"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"whistleblower","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/18/content_16631975.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Ecuador, CIA, Edward Snowden, PRISM'

        },
        {'start': 'Jun 13 2013 02:41:00 GMT-0100', 
        'title': 'WikiLeaks founder Assange to deliver speech',
        'classname': 'chinadaily',
        'graph': '{"id":122,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"WikiLeaks founder Assange to deliver speech","lon":151.20550537109375,"name":"Sydney","lat":-33.86149978637695}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16616381.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Julian Assange'

        },
        {'start': 'May 23 2013 04:14:00 GMT-0100', 
        'title': 'Intl data protection forum focuses on cybercrime',
        'classname': 'chinadaily',
        'graph': '{"id":123,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Intl data protection forum focuses on cybercrime","lon":131.90109252929688,"name":"Vladivostok","lat":43.12810134887695}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-05/23/content_16525678.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 13 2013 11:20:00 GMT-0100', 
        'title': 'Ex-CIA man says he exposed US spy scheme',
        'classname': 'chinadaily',
        'graph': '{"id":124,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16615081.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , CIA'

        },
        {'start': 'Sep 06 2013 04:44:00 GMT-0100', 
        'title': 'China expects further development at G20 summit',
        'classname': 'chinadaily',
        'graph': '{"id":125,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-09/06/content_16950418.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Russia, China'

        },
        {'start': 'Aug 30 2013 10:55:00 GMT-0100', 
        'title': 'US is abusing a powerful position',
        'classname': 'chinadaily',
        'graph': '{"id":126,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-08/30/content_16933775.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, '

        },
        {'start': 'Aug 28 2013 08:31:00 GMT-0100', 
        'title': 'Governments urged to use domestic software',
        'classname': 'chinadaily',
        'graph': '{"id":127,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-08/28/content_16928143.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , China'

        },
        {'start': 'Aug 22 2013 07:41:00 GMT-0100', 
        'title': 'US plays dubious role in arms control',
        'classname': 'chinadaily',
        'graph': '{"id":128,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-08/22/content_16912790.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Aug 20 2013 09:55:00 GMT-0100', 
        'title': 'China investigating foreign companies over security',
        'classname': 'chinadaily',
        'graph': '{"id":129,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-08/20/content_16909859.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , China'

        },
        {'start': 'Aug 19 2013 03:29:00 GMT-0100', 
        'title': 'Well-behaved intl firms welcomed in China',
        'classname': 'chinadaily',
        'graph': '{"id":130,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-08/19/content_16904698.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , China'

        },
        {'start': 'Aug 16 2013 09:07:00 GMT-0100', 
        'title': 'Chinese system in its own context',
        'classname': 'chinadaily',
        'graph': '{"id":131,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/epaper/2013-08/16/content_16898393.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Aug 15 2013 03:46:00 GMT-0100', 
        'title': 'Snowden case not to affect US-Russia talks',
        'classname': 'chinadaily',
        'graph': '{"id":132,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"10","size":2,"x":"33"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"6","size":2,"x":"15"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/15/content_16894782.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Aug 14 2013 07:55:00 GMT-0100', 
        'title': 'US, Russia may control rift',
        'classname': 'chinadaily',
        'graph': '{"id":133,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-08/14/content_16892155.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Russia'

        },
        {'start': 'Aug 13 2013 07:27:00 GMT-0100', 
        'title': 'Rules for cyberspace',
        'classname': 'chinadaily',
        'graph': '{"id":134,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-08/13/content_16889250.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Aug 06 2013 09:41:00 GMT-0100', 
        'title': 'Snowden obtains formal registration in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":135,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"92","size":2,"x":"35"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"7","size":2,"x":"46"}]}',
        'emo': '[{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/06/content_16875478.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Jul 31 2013 07:36:00 GMT-0100', 
        'title': 'Snowdens father tells son to stay in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":136,"edges":[{"id":"United StatesLon Snowden","weight":20,"source":"United States","target":"Lon Snowden","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesCIA","weight":20,"source":"United States","target":"CIA","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"Lon SnowdenEdward Snowden","weight":20,"source":"Lon Snowden","target":"Edward Snowden","label":""},{"id":"Lon SnowdenCIA","weight":20,"source":"Lon Snowden","target":"CIA","label":""},{"id":"Lon SnowdenRussia","weight":20,"source":"Lon Snowden","target":"Russia","label":""},{"id":"Edward SnowdenCIA","weight":20,"source":"Edward Snowden","target":"CIA","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"CIARussia","weight":20,"source":"CIA","target":"Russia","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"59","size":2,"x":"30"},{"id":"Lon Snowden","color":"rgb(102,153,255)","label":"Lon Snowden","y":"52","size":2,"x":"6"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"71","size":2,"x":"45"},{"id":"CIA","color":"rgb(102,153,255)","label":"CIA","y":"14","size":2,"x":"80"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"1","size":2,"x":"20"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/31/content_16858643.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Lon Snowden, CIA, Edward Snowden, Russia'

        },
        {'start': 'Jul 29 2013 09:34:00 GMT-0100', 
        'title': 'Snowden may stay in temporary shelter in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":137,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"87","size":2,"x":"23"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"13","size":2,"x":"66"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"39","size":2,"x":"97"}]}',
        'emo': '[{"term":"asylum","value":1},{"term":"whistleblower","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden may stay in temporary shelter in Russia","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/29/content_16849294.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia'

        },
        {'start': 'Jul 27 2013 03:03:00 GMT-0100', 
        'title': 'US assures no death penalty for Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":138,"edges":[{"id":"United StatesKremlin","weight":20,"source":"United States","target":"Kremlin","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"KremlinEdward Snowden","weight":20,"source":"Kremlin","target":"Edward Snowden","label":""},{"id":"KremlinRussia","weight":20,"source":"Kremlin","target":"Russia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"7","size":2,"x":"87"},{"id":"Kremlin","color":"rgb(102,153,255)","label":"Kremlin","y":"74","size":2,"x":"51"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"45","size":2,"x":"84"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"48","size":2,"x":"78"}]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"illegal","value":1},{"term":"death","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/27/content_16841676.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Kremlin, Edward Snowden, Russia'

        },
        {'start': 'Jul 24 2013 08:15:00 GMT-0100', 
        'title': 'Nation falling short on IT security: survey',
        'classname': 'chinadaily',
        'graph': '{"id":139,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/24/content_16821054.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 24 2013 08:15:00 GMT-0100', 
        'title': 'Nation falling short on IT security: survey',
        'classname': 'chinadaily',
        'graph': '{"id":140,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/24/content_16821175.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 23 2013 07:31:00 GMT-0100', 
        'title': 'Military transparency comes with trust',
        'classname': 'chinadaily',
        'graph': '{"id":141,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/23/content_16814608.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 13 2013 08:25:00 GMT-0100', 
        'title': 'Fallout of Snowden expose',
        'classname': 'chinadaily',
        'graph': '{"id":142,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/13/content_16770452.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 06 2013 10:44:00 GMT-0100', 
        'title': 'Venezuela offers asylum to Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":143,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowNicolas Maduro","weight":20,"source":"Moscow","target":"Nicolas Maduro","label":""},{"id":"Edward SnowdenNicolas Maduro","weight":20,"source":"Edward Snowden","target":"Nicolas Maduro","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"89","size":2,"x":"30"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"87","size":2,"x":"33"},{"id":"Nicolas Maduro","color":"rgb(102,153,255)","label":"Nicolas Maduro","y":"85","size":2,"x":"51"}]}',
        'emo': '[{"term":"asylum","value":1}]',
        'cities': '{"features":[{"city":{"title":"Venezuela offers asylum to Snowden","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/06/content_16740940.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Venezuela, Nicolas Maduro, Edward Snowden'

        },
        {'start': 'Jul 04 2013 08:37:00 GMT-0100', 
        'title': 'Snowden incident exposes mistrust between US and EU',
        'classname': 'chinadaily',
        'graph': '{"id":144,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/04/content_16730036.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, EU'

        },
        {'start': 'Jul 02 2013 02:47:00 GMT-0100', 
        'title': 'Mobile Internet broadens horizons',
        'classname': 'chinadaily',
        'graph': '{"id":145,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-07/02/content_16701514.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Internet'

        },
        {'start': 'Jul 01 2013 09:00:00 GMT-0100', 
        'title': 'Biased view of terrorism',
        'classname': 'chinadaily',
        'graph': '{"id":146,"edges":[],"nodes":[]}',
        'emo': '[{"term":"terrorism","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/01/content_16694197.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 27 2013 11:10:00 GMT-0100', 
        'title': 'Ecuador refutes Washington Post accusation',
        'classname': 'chinadaily',
        'graph': '{"id":147,"edges":[{"id":"EcuadorEdward Snowden","weight":20,"source":"Ecuador","target":"Edward Snowden","label":""},{"id":"EcuadorWashington Post","weight":20,"source":"Ecuador","target":"Washington Post","label":""},{"id":"EcuadorPRISM","weight":20,"source":"Ecuador","target":"PRISM","label":""},{"id":"Edward SnowdenWashington Post","weight":20,"source":"Edward Snowden","target":"Washington Post","label":""},{"id":"Edward SnowdenPRISM","weight":20,"source":"Edward Snowden","target":"PRISM","label":""},{"id":"Washington PostPRISM","weight":20,"source":"Washington Post","target":"PRISM","label":""}],"nodes":[{"id":"Ecuador","color":"rgb(102,153,255)","label":"Ecuador","y":"45","size":2,"x":"81"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"58","size":2,"x":"69"},{"id":"Washington Post","color":"rgb(102,153,255)","label":"Washington Post","y":"5","size":2,"x":"16"},{"id":"PRISM","color":"rgb(102,153,255)","label":"PRISM","y":"3","size":2,"x":"81"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Ecuador refutes Washington Post accusation","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/27/content_16670765.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Ecuador, Edward Snowden, Washington Post, PRISM'

        },
        {'start': 'Jun 23 2013 12:13:00 GMT-0100', 
        'title': 'Washington owes world explanations over spying accusations',
        'classname': 'chinadaily',
        'graph': '{"id":148,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[{"city":{"title":"Washington owes world explanations over spying accusations","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/23/content_16647574.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Jun 21 2013 10:10:00 GMT-0100', 
        'title': 'France, Spain take action against Google',
        'classname': 'chinadaily',
        'graph': '{"id":149,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/21/content_16643145.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: France, , Spain, Google'

        },
        {'start': 'Jun 20 2013 10:11:00 GMT-0100', 
        'title': 'Obama defends US intelligence strategy in Berlin',
        'classname': 'chinadaily',
        'graph': '{"id":150,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/20/content_16639830.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Berlin'

        },
        {'start': 'Jun 18 2013 02:02:00 GMT-0100', 
        'title': 'G8 kicks off amid controversy',
        'classname': 'chinadaily',
        'graph': '{"id":151,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/18/content_16631692.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Northern Ireland'

        },
        {'start': 'Jun 17 2013 02:29:00 GMT-0100', 
        'title': 'HK laws apply to Snowden case',
        'classname': 'chinadaily',
        'graph': '{"id":152,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/17/content_16628415.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 13 2013 07:44:00 GMT-0100', 
        'title': 'EU seeks answers on US data spying',
        'classname': 'chinadaily',
        'graph': '{"id":153,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2},{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16613000.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , EU'

        },
        {'start': 'May 18 2013 01:54:00 GMT-0100', 
        'title': 'Lis India trip shows pursuit of better ties',
        'classname': 'chinadaily',
        'graph': '{"id":154,"edges":[{"id":"BeijingNew Delhi","weight":20,"source":"Beijing","target":"New Delhi","label":""},{"id":"BeijingIndia","weight":20,"source":"Beijing","target":"India","label":""},{"id":"New DelhiIndia","weight":20,"source":"New Delhi","target":"India","label":""}],"nodes":[{"id":"Beijing","color":"rgb(102,153,255)","label":"Beijing","y":"92","size":2,"x":"10"},{"id":"New Delhi","color":"rgb(102,153,255)","label":"New Delhi","y":"39","size":2,"x":"39"},{"id":"India","color":"rgb(102,153,255)","label":"India","y":"55","size":2,"x":"41"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Lis India trip shows pursuit of better ties","lon":97.37460327148438,"name":"Delhi","lat":37.375999450683594}},{"city":{"title":"Lis India trip shows pursuit of better ties","lon":116.19869995117188,"name":"Beijing","lat":29.346399307250977}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-05/18/content_16509194.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , New Delhi, Beijing, India'

        },
        {'start': 'Sep 06 2013 04:10:00 GMT-0100', 
        'title': 'Brazil asks for apology from US on spying',
        'classname': 'chinadaily',
        'graph': '{"id":155,"edges":[{"id":"BrazilWhite House","weight":20,"source":"Brazil","target":"White House","label":""}],"nodes":[{"id":"Brazil","color":"rgb(102,153,255)","label":"Brazil","y":"5","size":2,"x":"93"},{"id":"White House","color":"rgb(102,153,255)","label":"White House","y":"20","size":2,"x":"34"}]}',
        'emo': '[{"term":"surveillance","value":1},{"term":"privacy","value":1},{"term":"public","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-09/06/content_16948119.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Brazil, White House'

        },
        {'start': 'Sep 07 2013 07:17:00 GMT-0100', 
        'title': 'US spy agencies decry latest Snowden revelations',
        'classname': 'chinadaily',
        'graph': '{"id":156,"edges":[],"nodes":[]}',
        'emo': '[{"term":"diclosure","value":2},{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-09/07/content_16951109.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Sep 04 2013 07:31:00 GMT-0100', 
        'title': 'Tower of Babel moment awaits NSA',
        'classname': 'chinadaily',
        'graph': '{"id":157,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-09/04/content_16942550.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Sep 03 2013 08:36:00 GMT-0100', 
        'title': 'Rousseff consults cabinet on US spy claims',
        'classname': 'chinadaily',
        'graph': '{"id":158,"edges":[],"nodes":[]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-09/03/content_16939263.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Brazil'

        },
        {'start': 'Aug 31 2013 12:43:00 GMT-0100', 
        'title': 'No Obama-Putin bilat at G20: official',
        'classname': 'chinadaily',
        'graph': '{"id":159,"edges":[{"id":"Barack ObamaRussia","weight":20,"source":"Barack Obama","target":"Russia","label":""},{"id":"Barack ObamaVladimir Putin","weight":20,"source":"Barack Obama","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"53","size":2,"x":"8"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"15","size":4,"x":"25"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"53","size":2,"x":"93"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/31/content_16934519.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Barack Obama, Russia, Vladimir Putin'

        },
        {'start': 'Sep 02 2013 03:32:00 GMT-0100', 
        'title': 'More illegal NSA spying activities leaked',
        'classname': 'chinadaily',
        'graph': '{"id":160,"edges":[{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"39","size":2,"x":"1"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"78","size":2,"x":"37"}]}',
        'emo': '[{"term":"surveillance","value":2},{"term":"illegal","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-09/02/content_16938124.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Edward Snowden'

        },
        {'start': 'Aug 31 2013 07:21:00 GMT-0100', 
        'title': 'UK asked NY Times to destroy Snowden material',
        'classname': 'chinadaily',
        'graph': '{"id":161,"edges":[{"id":"Edward SnowdenGCHQ","weight":20,"source":"Edward Snowden","target":"GCHQ","label":""},{"id":"Edward SnowdenNew York Times","weight":20,"source":"Edward Snowden","target":"New York Times","label":""},{"id":"GCHQNew York Times","weight":20,"source":"GCHQ","target":"New York Times","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"72","size":2,"x":"61"},{"id":"GCHQ","color":"rgb(102,153,255)","label":"GCHQ","y":"87","size":2,"x":"17"},{"id":"New York Times","color":"rgb(102,153,255)","label":"New York Times","y":"42","size":2,"x":"61"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"UK asked NY Times to destroy Snowden material","lon":-74.00599670410156,"name":"New York","lat":40.714298248291016}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/31/content_16933953.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, GCHQ, New York Times'

        },
        {'start': 'Aug 27 2013 06:04:00 GMT-0100', 
        'title': 'UN vows to contact US over spying report',
        'classname': 'chinadaily',
        'graph': '{"id":162,"edges":[{"id":"NSAUnited States","weight":20,"source":"NSA","target":"United States","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"1","size":2,"x":"91"},{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"93","size":2,"x":"68"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/27/content_16922447.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, NSA, '

        },
        {'start': 'Aug 22 2013 01:32:00 GMT-0100', 
        'title': 'US spying raises tensions with China',
        'classname': 'chinadaily',
        'graph': '{"id":163,"edges":[{"id":"BeijingChina","weight":20,"source":"Beijing","target":"China","label":""}],"nodes":[{"id":"Beijing","color":"rgb(102,153,255)","label":"Beijing","y":"82","size":2,"x":"16"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"86","size":2,"x":"10"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"US spying raises tensions with China","lon":116.19869995117188,"name":"Beijing","lat":29.346399307250977}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/22/content_16912304.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Beijing, China'

        },
        {'start': 'Aug 21 2013 01:15:00 GMT-0100', 
        'title': 'EU to investigate US survellance case',
        'classname': 'chinadaily',
        'graph': '{"id":164,"edges":[{"id":"EUEdward Snowden","weight":20,"source":"EU","target":"Edward Snowden","label":""},{"id":"EUPRISM","weight":20,"source":"EU","target":"PRISM","label":""},{"id":"Edward SnowdenPRISM","weight":20,"source":"Edward Snowden","target":"PRISM","label":""}],"nodes":[{"id":"EU","color":"rgb(102,153,255)","label":"EU","y":"81","size":2,"x":"76"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"37","size":2,"x":"77"},{"id":"PRISM","color":"rgb(102,153,255)","label":"PRISM","y":"96","size":2,"x":"65"}]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"surveillance","value":2},{"term":"privacy","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/21/content_16910887.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, EU, PRISM'

        },
        {'start': 'Aug 21 2013 02:57:00 GMT-0100', 
        'title': 'NSA surveillance covers 75% of US Internet traffic',
        'classname': 'chinadaily',
        'graph': '{"id":165,"edges":[{"id":"Wall Street JournalInternet","weight":20,"source":"Wall Street Journal","target":"Internet","label":""}],"nodes":[{"id":"Wall Street Journal","color":"rgb(102,153,255)","label":"Wall Street Journal","y":"2","size":2,"x":"58"},{"id":"Internet","color":"rgb(102,153,255)","label":"Internet","y":"57","size":2,"x":"4"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/21/content_16911243.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: Wall Street Journal, NSA, , Internet'

        },
        {'start': 'Aug 21 2013 09:19:00 GMT-0100', 
        'title': 'UK agents destroy Snowden files at Guardian',
        'classname': 'chinadaily',
        'graph': '{"id":166,"edges":[{"id":"Edward SnowdenThe Guardian","weight":20,"source":"Edward Snowden","target":"The Guardian","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"86","size":2,"x":"48"},{"id":"The Guardian","color":"rgb(102,153,255)","label":"The Guardian","y":"51","size":2,"x":"18"}]}',
        'emo': '[{"term":"press","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/21/content_16909877.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, The Guardian'

        },
        {'start': 'Aug 20 2013 06:34:00 GMT-0100', 
        'title': 'Snowden reporter to publish UK secrets',
        'classname': 'chinadaily',
        'graph': '{"id":167,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/20/content_16905853.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Aug 19 2013 11:28:00 GMT-0100', 
        'title': 'Partner of reporter at center of NSA leak detained',
        'classname': 'chinadaily',
        'graph': '{"id":168,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/19/content_16903975.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Edward Snowden'

        },
        {'start': 'Aug 16 2013 10:58:00 GMT-0100', 
        'title': 'Snowden downloaded NSA secrets while working for Dell',
        'classname': 'chinadaily',
        'graph': '{"id":169,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/16/content_16899228.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Edward Snowden'

        },
        {'start': 'Aug 10 2013 03:19:00 GMT-0100', 
        'title': 'Obama pledges more oversight over surveillance programs',
        'classname': 'chinadaily',
        'graph': '{"id":170,"edges":[{"id":"NSABarack Obama","weight":20,"source":"NSA","target":"Barack Obama","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"96","size":2,"x":"74"},{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"99","size":2,"x":"20"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/10/content_16885271.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Barack Obama'

        },
        {'start': 'Aug 14 2013 10:06:00 GMT-0100', 
        'title': 'Brazil demands clarifications on NSA surveillance',
        'classname': 'chinadaily',
        'graph': '{"id":171,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/14/content_16892807.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Brazil'

        },
        {'start': 'Aug 06 2013 07:34:00 GMT-0100', 
        'title': 'Snowden could be offered job at Russian parliament',
        'classname': 'chinadaily',
        'graph': '{"id":172,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/06/content_16875286.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Russia'

        },
        {'start': 'Aug 04 2013 05:16:00 GMT-0100', 
        'title': 'Germany ends information-sharing with US',
        'classname': 'chinadaily',
        'graph': '{"id":173,"edges":[{"id":"United StatesCold War","weight":20,"source":"United States","target":"Cold War","label":""},{"id":"United StatesGermany","weight":20,"source":"United States","target":"Germany","label":""},{"id":"Cold WarGermany","weight":20,"source":"Cold War","target":"Germany","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"62","size":2,"x":"64"},{"id":"Cold War","color":"rgb(102,153,255)","label":"Cold War","y":"46","size":2,"x":"92"},{"id":"Germany","color":"rgb(102,153,255)","label":"Germany","y":"65","size":2,"x":"66"}]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/04/content_16869045.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Cold War, Germany'

        },
        {'start': 'Aug 02 2013 07:42:00 GMT-0100', 
        'title': 'Snowden granted 1 years temporary asylum in Russia',
        'classname': 'chinadaily',
        'graph': '{"id":174,"edges":[{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"23","size":2,"x":"43"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"72","size":2,"x":"52"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"whistleblower","value":1},{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/02/content_16864254.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Russia'

        },
        {'start': 'Aug 02 2013 10:08:00 GMT-0100', 
        'title': 'NSA chief details program at hackers conference',
        'classname': 'chinadaily',
        'graph': '{"id":175,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/02/content_16865193.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Aug 02 2013 10:06:00 GMT-0100', 
        'title': 'New Snowden leak upstages US',
        'classname': 'chinadaily',
        'graph': '{"id":176,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/02/content_16865168.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Aug 01 2013 06:59:00 GMT-0100', 
        'title': 'Obama declassifies surveillance program files',
        'classname': 'chinadaily',
        'graph': '{"id":177,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-08/01/content_16859554.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 26 2013 08:02:00 GMT-0100', 
        'title': 'Congress approves NSA spying program',
        'classname': 'chinadaily',
        'graph': '{"id":178,"edges":[],"nodes":[]}',
        'emo': '[{"term":"terrorism","value":1},{"term":"surveillance","value":1},{"term":"privacy","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/26/content_16833775.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Jul 25 2013 02:35:00 GMT-0100', 
        'title': 'US House rejects to curb spy agency data collection',
        'classname': 'chinadaily',
        'graph': '{"id":179,"edges":[{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"4","size":2,"x":"67"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"86","size":2,"x":"57"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/25/content_16830778.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Edward Snowden'

        },
        {'start': 'Jul 23 2013 09:52:00 GMT-0100', 
        'title': 'Germany to probe spy services ties with US',
        'classname': 'chinadaily',
        'graph': '{"id":180,"edges":[{"id":"NSAUnited States","weight":20,"source":"NSA","target":"United States","label":""},{"id":"NSAGermany","weight":20,"source":"NSA","target":"Germany","label":""},{"id":"United StatesGermany","weight":20,"source":"United States","target":"Germany","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"57","size":2,"x":"13"},{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"60","size":2,"x":"37"},{"id":"Germany","color":"rgb(102,153,255)","label":"Germany","y":"35","size":2,"x":"82"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/23/content_16815671.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, NSA, , Germany'

        },
        {'start': 'Jul 20 2013 07:55:00 GMT-0100', 
        'title': 'Venezuela VP defends support for Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":181,"edges":[{"id":"VenezuelaEdward Snowden","weight":20,"source":"Venezuela","target":"Edward Snowden","label":""}],"nodes":[{"id":"Venezuela","color":"rgb(102,153,255)","label":"Venezuela","y":"4","size":2,"x":"86"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"40","size":2,"x":"89"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/20/content_16804314.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Venezuela, Edward Snowden'

        },
        {'start': 'Jul 19 2013 07:23:00 GMT-0100', 
        'title': 'NSA implements new security measures',
        'classname': 'chinadaily',
        'graph': '{"id":182,"edges":[{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"85","size":2,"x":"5"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"13","size":2,"x":"48"}]}',
        'emo': '[{"term":"diclosure","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/19/content_16796898.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Edward Snowden'

        },
        {'start': 'Jul 19 2013 07:40:00 GMT-0100', 
        'title': 'Pentagon to field 4,000-person cyber squad',
        'classname': 'chinadaily',
        'graph': '{"id":183,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/19/content_16796961.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: Pentagon, '

        },
        {'start': 'Jul 17 2013 09:54:00 GMT-0100', 
        'title': 'Change drives China-US talks',
        'classname': 'chinadaily',
        'graph': '{"id":184,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/17/content_16789182.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , China'

        },
        {'start': 'Jul 17 2013 09:54:00 GMT-0100', 
        'title': 'Change drives China-US talks',
        'classname': 'chinadaily',
        'graph': '{"id":185,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/17/content_16787099.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , China'

        },
        {'start': 'Jul 16 2013 07:41:00 GMT-0100', 
        'title': 'Putin wants Snowden to go, but asylum not ruled out',
        'classname': 'chinadaily',
        'graph': '{"id":186,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"MoscowVladimir Putin","weight":20,"source":"Moscow","target":"Vladimir Putin","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVladimir Putin","weight":20,"source":"Edward Snowden","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"40","size":2,"x":"21"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"27","size":2,"x":"43"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"22","size":2,"x":"74"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"44","size":2,"x":"45"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Putin wants Snowden to go, but asylum not ruled out","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/16/content_16780494.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia, Vladimir Putin'

        },
        {'start': 'Jul 10 2013 10:38:00 GMT-0100', 
        'title': 'Brazil denies Snowden asylum',
        'classname': 'chinadaily',
        'graph': '{"id":187,"edges":[{"id":"BrazilEdward Snowden","weight":20,"source":"Brazil","target":"Edward Snowden","label":""}],"nodes":[{"id":"Brazil","color":"rgb(102,153,255)","label":"Brazil","y":"31","size":2,"x":"23"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"43","size":2,"x":"0"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/10/content_16756041.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Brazil, Edward Snowden'

        },
        {'start': 'Jul 10 2013 08:52:00 GMT-0100', 
        'title': 'China, US hold talks on cyber security',
        'classname': 'chinadaily',
        'graph': '{"id":188,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-07/10/content_16755122.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Economic Dialogue, China'

        },
        {'start': 'Jul 09 2013 09:20:00 GMT-0100', 
        'title': 'US ambassador denies Brazils spying accusation',
        'classname': 'chinadaily',
        'graph': '{"id":189,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/09/content_16750359.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Brazil'

        },
        {'start': 'Jul 09 2013 08:27:00 GMT-0100', 
        'title': 'Germany defends strictly legal co-op with NSA',
        'classname': 'chinadaily',
        'graph': '{"id":190,"edges":[],"nodes":[]}',
        'emo': '[{"term":"legal","value":1},{"term":"press","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/09/content_16749879.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Germany, Angela Merkel'

        },
        {'start': 'Jul 08 2013 07:15:00 GMT-0100', 
        'title': 'Level the playing field',
        'classname': 'chinadaily',
        'graph': '{"id":191,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/08/content_16744586.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 06 2013 08:35:00 GMT-0100', 
        'title': 'Nicaragua willing to grant Snowden asylum',
        'classname': 'chinadaily',
        'graph': '{"id":192,"edges":[{"id":"Edward SnowdenNicaragua","weight":20,"source":"Edward Snowden","target":"Nicaragua","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"86","size":2,"x":"56"},{"id":"Nicaragua","color":"rgb(102,153,255)","label":"Nicaragua","y":"10","size":2,"x":"80"}]}',
        'emo': '[{"term":"asylum","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/06/content_16740512.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, Nicaragua'

        },
        {'start': 'Jul 05 2013 10:36:00 GMT-0100', 
        'title': 'Snowden gets proposal tweet from Russian agent',
        'classname': 'chinadaily',
        'graph': '{"id":193,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"MoscowVladimir Putin","weight":20,"source":"Moscow","target":"Vladimir Putin","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVladimir Putin","weight":20,"source":"Edward Snowden","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"43","size":2,"x":"3"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"82","size":2,"x":"55"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"36","size":2,"x":"78"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"64","size":2,"x":"58"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden gets proposal tweet from Russian agent","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/05/content_16735857.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden, Russia, Vladimir Putin'

        },
        {'start': 'Jul 04 2013 07:43:00 GMT-0100', 
        'title': 'Rally held in support of Snowden in Berlin',
        'classname': 'chinadaily',
        'graph': '{"id":194,"edges":[{"id":"NSABerlin","weight":20,"source":"NSA","target":"Berlin","label":""},{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""},{"id":"BerlinEdward Snowden","weight":20,"source":"Berlin","target":"Edward Snowden","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"85","size":2,"x":"81"},{"id":"Berlin","color":"rgb(102,153,255)","label":"Berlin","y":"55","size":2,"x":"19"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"61","size":2,"x":"52"}]}',
        'emo': '[{"term":"surveillance","value":1},{"term":"protest","value":1}]',
        'cities': '{"features":[{"city":{"title":"Rally held in support of Snowden in Berlin","lon":13.399999618530273,"name":"Berlin","lat":52.516700744628906}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/04/content_16729834.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Berlin, Edward Snowden'

        },
        {'start': 'Jul 04 2013 03:28:00 GMT-0100', 
        'title': 'Domestic software sees opportunity in PRISM',
        'classname': 'chinadaily',
        'graph': '{"id":195,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Domestic software sees opportunity in PRISM","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/04/content_16728093.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , PRISM'

        },
        {'start': 'Jul 04 2013 10:22:00 GMT-0100', 
        'title': 'Its up to citizens to save US',
        'classname': 'chinadaily',
        'graph': '{"id":196,"edges":[{"id":"Edward SnowdenCIA","weight":20,"source":"Edward Snowden","target":"CIA","label":""},{"id":"Edward SnowdenInternet","weight":20,"source":"Edward Snowden","target":"Internet","label":""},{"id":"CIAInternet","weight":20,"source":"CIA","target":"Internet","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"79","size":2,"x":"84"},{"id":"CIA","color":"rgb(102,153,255)","label":"CIA","y":"86","size":2,"x":"5"},{"id":"Internet","color":"rgb(102,153,255)","label":"Internet","y":"11","size":2,"x":"16"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Its up to citizens to save US","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/04/content_16725129.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , CIA, Edward Snowden, Internet'

        },
        {'start': 'Jul 03 2013 03:49:00 GMT-0100', 
        'title': 'Snowden fears diverts Bolivian presidents plane',
        'classname': 'chinadaily',
        'graph': '{"id":197,"edges":[{"id":"Evo MoralesAustria","weight":20,"source":"Evo Morales","target":"Austria","label":""},{"id":"Evo MoralesEdward Snowden","weight":20,"source":"Evo Morales","target":"Edward Snowden","label":""},{"id":"Evo MoralesRussia","weight":20,"source":"Evo Morales","target":"Russia","label":""},{"id":"Evo MoralesBolivia","weight":20,"source":"Evo Morales","target":"Bolivia","label":""},{"id":"AustriaEdward Snowden","weight":20,"source":"Austria","target":"Edward Snowden","label":""},{"id":"AustriaRussia","weight":20,"source":"Austria","target":"Russia","label":""},{"id":"AustriaBolivia","weight":20,"source":"Austria","target":"Bolivia","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenBolivia","weight":20,"source":"Edward Snowden","target":"Bolivia","label":""},{"id":"RussiaBolivia","weight":20,"source":"Russia","target":"Bolivia","label":""}],"nodes":[{"id":"Evo Morales","color":"rgb(102,153,255)","label":"Evo Morales","y":"66","size":2,"x":"60"},{"id":"Austria","color":"rgb(102,153,255)","label":"Austria","y":"44","size":2,"x":"95"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"19","size":2,"x":"64"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"37","size":2,"x":"71"},{"id":"Bolivia","color":"rgb(102,153,255)","label":"Bolivia","y":"52","size":2,"x":"54"}]}',
        'emo': '[{"term":"asylum","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/03/content_16718256.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Evo Morales, Edward Snowden, Austria, Russia, Bolivia'

        },
        {'start': 'Jul 03 2013 07:21:00 GMT-0100', 
        'title': 'Venezuela eyed as Snowden seeks asylum',
        'classname': 'chinadaily',
        'graph': '{"id":198,"edges":[{"id":"NSAUnited States","weight":20,"source":"NSA","target":"United States","label":""},{"id":"NSAMoscow","weight":20,"source":"NSA","target":"Moscow","label":""},{"id":"NSAVenezuela","weight":20,"source":"NSA","target":"Venezuela","label":""},{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""},{"id":"NSARussia","weight":20,"source":"NSA","target":"Russia","label":""},{"id":"NSAVladimir Putin","weight":20,"source":"NSA","target":"Vladimir Putin","label":""},{"id":"United StatesMoscow","weight":20,"source":"United States","target":"Moscow","label":""},{"id":"United StatesVenezuela","weight":20,"source":"United States","target":"Venezuela","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesRussia","weight":20,"source":"United States","target":"Russia","label":""},{"id":"United StatesVladimir Putin","weight":20,"source":"United States","target":"Vladimir Putin","label":""},{"id":"MoscowVenezuela","weight":20,"source":"Moscow","target":"Venezuela","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"MoscowVladimir Putin","weight":20,"source":"Moscow","target":"Vladimir Putin","label":""},{"id":"VenezuelaEdward Snowden","weight":20,"source":"Venezuela","target":"Edward Snowden","label":""},{"id":"VenezuelaRussia","weight":20,"source":"Venezuela","target":"Russia","label":""},{"id":"VenezuelaVladimir Putin","weight":20,"source":"Venezuela","target":"Vladimir Putin","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"Edward SnowdenVladimir Putin","weight":20,"source":"Edward Snowden","target":"Vladimir Putin","label":""},{"id":"RussiaVladimir Putin","weight":20,"source":"Russia","target":"Vladimir Putin","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"96","size":2,"x":"45"},{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"72","size":2,"x":"39"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"80","size":2,"x":"41"},{"id":"Venezuela","color":"rgb(102,153,255)","label":"Venezuela","y":"30","size":2,"x":"18"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"80","size":2,"x":"95"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"73","size":2,"x":"90"},{"id":"Vladimir Putin","color":"rgb(102,153,255)","label":"Vladimir Putin","y":"47","size":2,"x":"2"}]}',
        'emo': '[{"term":"asylum","value":1}]',
        'cities': '{"features":[{"city":{"title":"Venezuela eyed as Snowden seeks asylum","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/03/content_16712105.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, NSA, , Moscow, Venezuela, Edward Snowden, Russia, Vladimir Putin'

        },
        {'start': 'Jul 03 2013 05:45:00 GMT-0100', 
        'title': 'Sinovel to divest overseas units',
        'classname': 'chinadaily',
        'graph': '{"id":199,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-07/03/content_16713960.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jul 02 2013 06:53:00 GMT-0100', 
        'title': 'ROK asks US to confirm spying claims',
        'classname': 'chinadaily',
        'graph': '{"id":200,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/02/content_16709806.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, '

        },
        {'start': 'Jul 02 2013 05:59:00 GMT-0100', 
        'title': 'Snowden threats to make new US leaks',
        'classname': 'chinadaily',
        'graph': '{"id":201,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"87","size":2,"x":"44"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"93","size":2,"x":"18"}]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"freedom","value":1},{"term":"surveillance","value":2}]',
        'cities': '{"features":[{"city":{"title":"Snowden threats to make new US leaks","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/02/content_16702055.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden'

        },
        {'start': 'Jul 01 2013 06:41:00 GMT-0100', 
        'title': 'New NSA spying allegations rile European allies',
        'classname': 'chinadaily',
        'graph': '{"id":202,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/01/content_16693457.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Jul 01 2013 06:41:00 GMT-0100', 
        'title': 'New NSA spying allegations rile European allies',
        'classname': 'chinadaily',
        'graph': '{"id":203,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-07/01/content_16692919.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Jul 01 2013 08:45:00 GMT-0100', 
        'title': 'Snowdens whereabout',
        'classname': 'chinadaily',
        'graph': '{"id":204,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-07/01/content_16693839.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 30 2013 09:40:00 GMT-0100', 
        'title': 'Secret documents show NSA spying on EU offices',
        'classname': 'chinadaily',
        'graph': '{"id":205,"edges":[{"id":"NSAEU","weight":20,"source":"NSA","target":"EU","label":""},{"id":"NSADer Spiegel","weight":20,"source":"NSA","target":"Der Spiegel","label":""},{"id":"EUDer Spiegel","weight":20,"source":"EU","target":"Der Spiegel","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"67","size":2,"x":"10"},{"id":"EU","color":"rgb(102,153,255)","label":"EU","y":"6","size":2,"x":"25"},{"id":"Der Spiegel","color":"rgb(102,153,255)","label":"Der Spiegel","y":"85","size":2,"x":"66"}]}',
        'emo': '[{"term":"press","value":1},{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/30/content_16690294.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , EU, Der Spiegel'

        },
        {'start': 'Jun 28 2013 08:14:00 GMT-0100', 
        'title': 'Dodging key question on Snowdens revelations',
        'classname': 'chinadaily',
        'graph': '{"id":206,"edges":[],"nodes":[]}',
        'emo': '[{"term":"diclosure","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/28/content_16886898.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 28 2013 10:26:00 GMT-0100', 
        'title': 'US media falls silent on tapping and privacy',
        'classname': 'chinadaily',
        'graph': '{"id":207,"edges":[],"nodes":[]}',
        'emo': '[{"term":"privacy","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/epaper/2013-06/28/content_16679615.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 28 2013 06:51:00 GMT-0100', 
        'title': 'US collects Internet data on citizens',
        'classname': 'chinadaily',
        'graph': '{"id":208,"edges":[{"id":"NSAInternet","weight":20,"source":"NSA","target":"Internet","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"81","size":2,"x":"84"},{"id":"Internet","color":"rgb(102,153,255)","label":"Internet","y":"88","size":2,"x":"99"}]}',
        'emo': '[{"term":"press","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/28/content_16676994.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Internet'

        },
        {'start': 'Jun 26 2013 06:51:00 GMT-0100', 
        'title': 'Its time the US accepted reality',
        'classname': 'chinadaily',
        'graph': '{"id":209,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-06/26/content_16664783.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 26 2013 06:51:00 GMT-0100', 
        'title': 'Its time the US accepted reality',
        'classname': 'chinadaily',
        'graph': '{"id":210,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/26/content_16659264.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 24 2013 01:12:00 GMT-0100', 
        'title': 'US stole Chinese statistics',
        'classname': 'chinadaily',
        'graph': '{"id":211,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/24/content_16648272.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Internet'

        },
        {'start': 'Jun 24 2013 04:06:00 GMT-0100', 
        'title': 'Ecuador receives asylum request from Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":212,"edges":[{"id":"EcuadorEdward Snowden","weight":20,"source":"Ecuador","target":"Edward Snowden","label":""},{"id":"EcuadorTwitter","weight":20,"source":"Ecuador","target":"Twitter","label":""},{"id":"Edward SnowdenTwitter","weight":20,"source":"Edward Snowden","target":"Twitter","label":""}],"nodes":[{"id":"Ecuador","color":"rgb(102,153,255)","label":"Ecuador","y":"67","size":2,"x":"0"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"61","size":2,"x":"88"},{"id":"Twitter","color":"rgb(102,153,255)","label":"Twitter","y":"41","size":2,"x":"11"}]}',
        'emo': '[{"term":"asylum","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/24/content_16648518.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Ecuador, Edward Snowden, Twitter'

        },
        {'start': 'Jun 24 2013 04:23:00 GMT-0100', 
        'title': 'Snowden to leave Moscow for Cuba: media',
        'classname': 'chinadaily',
        'graph': '{"id":213,"edges":[{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"41","size":2,"x":"24"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"11","size":2,"x":"36"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Snowden to leave Moscow for Cuba: media","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/24/content_16652402.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Edward Snowden'

        },
        {'start': 'Jun 24 2013 07:15:00 GMT-0100', 
        'title': 'On verge of a once-in-a-century change',
        'classname': 'chinadaily',
        'graph': '{"id":214,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/24/content_16648893.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 23 2013 07:31:00 GMT-0100', 
        'title': 'Snowden may be on transit flight via Moscow',
        'classname': 'chinadaily',
        'graph': '{"id":215,"edges":[{"id":"MoscowForeign Ministry","weight":20,"source":"Moscow","target":"Foreign Ministry","label":""},{"id":"MoscowEdward Snowden","weight":20,"source":"Moscow","target":"Edward Snowden","label":""},{"id":"MoscowCIA","weight":20,"source":"Moscow","target":"CIA","label":""},{"id":"MoscowRussia","weight":20,"source":"Moscow","target":"Russia","label":""},{"id":"Foreign MinistryEdward Snowden","weight":20,"source":"Foreign Ministry","target":"Edward Snowden","label":""},{"id":"Foreign MinistryCIA","weight":20,"source":"Foreign Ministry","target":"CIA","label":""},{"id":"Foreign MinistryRussia","weight":20,"source":"Foreign Ministry","target":"Russia","label":""},{"id":"Edward SnowdenCIA","weight":20,"source":"Edward Snowden","target":"CIA","label":""},{"id":"Edward SnowdenRussia","weight":20,"source":"Edward Snowden","target":"Russia","label":""},{"id":"CIARussia","weight":20,"source":"CIA","target":"Russia","label":""}],"nodes":[{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"74","size":2,"x":"86"},{"id":"Foreign Ministry","color":"rgb(102,153,255)","label":"Foreign Ministry","y":"8","size":2,"x":"92"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"16","size":2,"x":"80"},{"id":"CIA","color":"rgb(102,153,255)","label":"CIA","y":"38","size":2,"x":"18"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"25","size":2,"x":"38"}]}',
        'emo': '[]',
        'cities': '{"features":[{"city":{"title":"Snowden may be on transit flight via Moscow","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/23/content_16648000.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Moscow, Foreign Ministry, CIA, Edward Snowden, Russia'

        },
        {'start': 'Jun 23 2013 11:37:00 GMT-0100', 
        'title': 'Flight reportedly carrying Snowden lands in Moscow',
        'classname': 'chinadaily',
        'graph': '{"id":216,"edges":[{"id":"United StatesAeroflot","weight":20,"source":"United States","target":"Aeroflot","label":""},{"id":"United StatesSheremetyevo","weight":20,"source":"United States","target":"Sheremetyevo","label":""},{"id":"United StatesMoscow","weight":20,"source":"United States","target":"Moscow","label":""},{"id":"AeroflotSheremetyevo","weight":20,"source":"Aeroflot","target":"Sheremetyevo","label":""},{"id":"AeroflotMoscow","weight":20,"source":"Aeroflot","target":"Moscow","label":""},{"id":"SheremetyevoMoscow","weight":20,"source":"Sheremetyevo","target":"Moscow","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"76","size":2,"x":"59"},{"id":"Aeroflot","color":"rgb(102,153,255)","label":"Aeroflot","y":"28","size":2,"x":"2"},{"id":"Sheremetyevo","color":"rgb(102,153,255)","label":"Sheremetyevo","y":"30","size":2,"x":"74"},{"id":"Moscow","color":"rgb(102,153,255)","label":"Moscow","y":"82","size":2,"x":"17"}]}',
        'emo': '[{"term":"diclosure","value":1},{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Flight reportedly carrying Snowden lands in Moscow","lon":37.6156005859375,"name":"Moscow","lat":55.752201080322266}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/23/content_16648253.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Aeroflot, Moscow, Sheremetyevo'

        },
        {'start': 'Jun 23 2013 04:25:00 GMT-0100', 
        'title': 'US seeks Snowdens extradition from HK',
        'classname': 'chinadaily',
        'graph': '{"id":217,"edges":[{"id":"United StatesHong Kong","weight":20,"source":"United States","target":"Hong Kong","label":""},{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"84","size":2,"x":"11"},{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"72","size":2,"x":"74"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"27","size":2,"x":"74"}]}',
        'emo': '[{"term":"legal","value":1}]',
        'cities': '{"features":[{"city":{"title":"US seeks Snowdens extradition from HK","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/23/content_16647853.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Hong Kong, Edward Snowden'

        },
        {'start': 'Jun 22 2013 08:24:00 GMT-0100', 
        'title': 'US medias disconnect in Snowden case',
        'classname': 'chinadaily',
        'graph': '{"id":218,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/22/content_16646037.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 21 2013 10:57:00 GMT-0100', 
        'title': 'Keep Sino-US ties on the right track',
        'classname': 'chinadaily',
        'graph': '{"id":219,"edges":[{"id":"United StatesChina","weight":20,"source":"United States","target":"China","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"20","size":2,"x":"52"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"52","size":2,"x":"67"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/21/content_16645590.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , China'

        },
        {'start': 'Jun 21 2013 08:56:00 GMT-0100', 
        'title': 'IN BRIEF (Page 3)',
        'classname': 'chinadaily',
        'graph': '{"id":220,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/epaper/2013-06/21/content_16642518.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 21 2013 03:36:00 GMT-0100', 
        'title': 'Snowdens future hangs in the balance',
        'classname': 'chinadaily',
        'graph': '{"id":221,"edges":[{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"47","size":2,"x":"9"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"8","size":2,"x":"84"}]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/21/content_16642135.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Edward Snowden'

        },
        {'start': 'Jun 21 2013 08:05:00 GMT-0100', 
        'title': 'Snowden has exposed the obvious',
        'classname': 'chinadaily',
        'graph': '{"id":222,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/21/content_16642424.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 20 2013 09:48:00 GMT-0100', 
        'title': 'FBI says it uses surveillance drones on US soil',
        'classname': 'chinadaily',
        'graph': '{"id":223,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/20/content_16639595.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , FBI'

        },
        {'start': 'Jun 20 2013 08:43:00 GMT-0100', 
        'title': 'FBI uses surveillance drones on US soil',
        'classname': 'chinadaily',
        'graph': '{"id":224,"edges":[{"id":"United StatesFBI","weight":20,"source":"United States","target":"FBI","label":""},{"id":"United StatesBarack Obama","weight":20,"source":"United States","target":"Barack Obama","label":""},{"id":"FBIBarack Obama","weight":20,"source":"FBI","target":"Barack Obama","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"83","size":2,"x":"8"},{"id":"FBI","color":"rgb(102,153,255)","label":"FBI","y":"95","size":2,"x":"18"},{"id":"Barack Obama","color":"rgb(102,153,255)","label":"Barack Obama","y":"12","size":2,"x":"88"}]}',
        'emo': '[{"term":"surveillance","value":3}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/20/content_16639140.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , FBI, Barack Obama'

        },
        {'start': 'Jun 20 2013 08:33:00 GMT-0100', 
        'title': 'In defense of Edward Snowden',
        'classname': 'chinadaily',
        'graph': '{"id":225,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/20/content_16639135.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden'

        },
        {'start': 'Jun 19 2013 07:42:00 GMT-0100', 
        'title': 'Cisco denies China monitoring accusations',
        'classname': 'chinadaily',
        'graph': '{"id":226,"edges":[{"id":"Edward SnowdenChina","weight":20,"source":"Edward Snowden","target":"China","label":""}],"nodes":[{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"46","size":2,"x":"41"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"30","size":2,"x":"81"}]}',
        'emo': '[{"term":"surveillance","value":1},{"term":"illegal","value":1},{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/business/2013-06/19/content_16636041.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Edward Snowden, China'

        },
        {'start': 'Jun 19 2013 09:44:00 GMT-0100', 
        'title': 'Obama defends US internet surveillance',
        'classname': 'chinadaily',
        'graph': '{"id":227,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/19/content_16638595.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Barack Obama'

        },
        {'start': 'Jun 19 2013 09:13:00 GMT-0100', 
        'title': 'US surveillance to overshadow Obamas Berlin trip',
        'classname': 'chinadaily',
        'graph': '{"id":228,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"US surveillance to overshadow Obamas Berlin trip","lon":13.399999618530273,"name":"Berlin","lat":52.516700744628906}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/19/content_16635964.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Berlin'

        },
        {'start': 'Jun 19 2013 08:46:00 GMT-0100', 
        'title': 'Snowden storm stains US',
        'classname': 'chinadaily',
        'graph': '{"id":229,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/19/content_16635764.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 19 2013 08:55:00 GMT-0100', 
        'title': 'A case of the thief crying, Stop! Thief!',
        'classname': 'chinadaily',
        'graph': '{"id":230,"edges":[{"id":"United StatesEdward Snowden","weight":20,"source":"United States","target":"Edward Snowden","label":""},{"id":"United StatesChina","weight":20,"source":"United States","target":"China","label":""},{"id":"Edward SnowdenChina","weight":20,"source":"Edward Snowden","target":"China","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"2","size":2,"x":"24"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"77","size":2,"x":"5"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"66","size":2,"x":"86"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/19/content_16635769.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Edward Snowden, China'

        },
        {'start': 'Jun 19 2013 09:14:00 GMT-0100', 
        'title': 'Obamas Syria policy',
        'classname': 'chinadaily',
        'graph': '{"id":231,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/19/content_16635966.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Syria'

        },
        {'start': 'Jun 18 2013 11:00:00 GMT-0100', 
        'title': 'Snowden hits back against critics of NSA leaks',
        'classname': 'chinadaily',
        'graph': '{"id":232,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/18/content_16632908.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Internet'

        },
        {'start': 'Jun 18 2013 07:58:00 GMT-0100', 
        'title': 'Information highway or spy-way?',
        'classname': 'chinadaily',
        'graph': '{"id":233,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/18/content_16631872.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 18 2013 01:54:00 GMT-0100', 
        'title': 'Snowden spying claims rejected',
        'classname': 'chinadaily',
        'graph': '{"id":234,"edges":[{"id":"NSAForeign Ministry","weight":20,"source":"NSA","target":"Foreign Ministry","label":""},{"id":"NSAEdward Snowden","weight":20,"source":"NSA","target":"Edward Snowden","label":""},{"id":"NSABeijing","weight":20,"source":"NSA","target":"Beijing","label":""},{"id":"Foreign MinistryEdward Snowden","weight":20,"source":"Foreign Ministry","target":"Edward Snowden","label":""},{"id":"Foreign MinistryBeijing","weight":20,"source":"Foreign Ministry","target":"Beijing","label":""},{"id":"Edward SnowdenBeijing","weight":20,"source":"Edward Snowden","target":"Beijing","label":""}],"nodes":[{"id":"NSA","color":"rgb(102,153,255)","label":"NSA","y":"47","size":2,"x":"96"},{"id":"Foreign Ministry","color":"rgb(102,153,255)","label":"Foreign Ministry","y":"86","size":2,"x":"80"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"16","size":2,"x":"17"},{"id":"Beijing","color":"rgb(102,153,255)","label":"Beijing","y":"35","size":2,"x":"40"}]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[{"city":{"title":"Snowden spying claims rejected","lon":116.19869995117188,"name":"Beijing","lat":29.346399307250977}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/18/content_16631688.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Foreign Ministry, Beijing, Edward Snowden'

        },
        {'start': 'Jun 16 2013 10:40:00 GMT-0100', 
        'title': 'Officials: NSA programs broke plots in 20 nations',
        'classname': 'chinadaily',
        'graph': '{"id":235,"edges":[],"nodes":[]}',
        'emo': '[{"term":"terrorism","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/16/content_16627263.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Jun 15 2013 08:51:00 GMT-0100', 
        'title': 'HK to handle Snowdens case according to laws',
        'classname': 'chinadaily',
        'graph': '{"id":236,"edges":[{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"79","size":2,"x":"10"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"16","size":2,"x":"65"}]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[{"city":{"title":"HK to handle Snowdens case according to laws","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/15/content_16626374.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, Edward Snowden'

        },
        {'start': 'Jun 14 2013 03:17:00 GMT-0100', 
        'title': 'Whistleblower pulls US into confidence crisis',
        'classname': 'chinadaily',
        'graph': '{"id":237,"edges":[],"nodes":[]}',
        'emo': '[{"term":"whistleblower","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/14/content_16622276.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 13 2013 11:14:00 GMT-0100', 
        'title': 'US spy agency seeks criminal probe into NSA leak',
        'classname': 'chinadaily',
        'graph': '{"id":238,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16614970.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Jun 13 2013 01:04:00 GMT-0100', 
        'title': 'NSA leaker: Im neither traitor nor hero',
        'classname': 'chinadaily',
        'graph': '{"id":239,"edges":[{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"24","size":2,"x":"92"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"25","size":2,"x":"48"}]}',
        'emo': '[{"term":"hero","value":2},{"term":"traitor","value":2},{"term":"whistleblower","value":1}]',
        'cities': '{"features":[{"city":{"title":"NSA leaker: Im neither traitor nor hero","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16615357.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Hong Kong, Edward Snowden'

        },
        {'start': 'Jun 14 2013 07:10:00 GMT-0100', 
        'title': 'US FBI chief defends surveillance programs',
        'classname': 'chinadaily',
        'graph': '{"id":240,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/14/content_16618516.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , FBI'

        },
        {'start': 'Jun 14 2013 01:16:00 GMT-0100', 
        'title': 'US should explain hacking activity',
        'classname': 'chinadaily',
        'graph': '{"id":241,"edges":[{"id":"United StatesBeijing","weight":20,"source":"United States","target":"Beijing","label":""},{"id":"United StatesChina","weight":20,"source":"United States","target":"China","label":""},{"id":"BeijingChina","weight":20,"source":"Beijing","target":"China","label":""}],"nodes":[{"id":"United States","color":"rgb(102,153,255)","label":"United States","y":"55","size":2,"x":"50"},{"id":"Beijing","color":"rgb(102,153,255)","label":"Beijing","y":"72","size":2,"x":"41"},{"id":"China","color":"rgb(102,153,255)","label":"China","y":"51","size":2,"x":"78"}]}',
        'emo': '[{"term":"hacker","value":2}]',
        'cities': '{"features":[{"city":{"title":"US should explain hacking activity","lon":116.19869995117188,"name":"Beijing","lat":29.346399307250977}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/14/content_16618098.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: United States, , Beijing, China'

        },
        {'start': 'Jun 13 2013 10:37:00 GMT-0100', 
        'title': 'US NSA chief defends surveillance programs',
        'classname': 'chinadaily',
        'graph': '{"id":242,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16614645.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Jun 13 2013 06:56:00 GMT-0100', 
        'title': 'Whistle-blower talks about himself',
        'classname': 'chinadaily',
        'graph': '{"id":243,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16612874.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 13 2013 12:34:00 GMT-0100', 
        'title': 'UK: eavesdropping is legal, defends US spy links',
        'classname': 'chinadaily',
        'graph': '{"id":244,"edges":[],"nodes":[]}',
        'emo': '[{"term":"legal","value":1},{"term":"surveillance","value":3},{"term":"privacy","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16615260.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , GCHQ'

        },
        {'start': 'Jun 13 2013 09:40:00 GMT-0100', 
        'title': 'US hacking China for years: Report',
        'classname': 'chinadaily',
        'graph': '{"id":245,"edges":[{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"78","size":2,"x":"30"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"63","size":2,"x":"22"}]}',
        'emo': '[{"term":"whistleblower","value":1},{"term":"hacker","value":2}]',
        'cities': '{"features":[{"city":{"title":"US hacking China for years: Report","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/13/content_16613864.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, Edward Snowden, China'

        },
        {'start': 'Jun 13 2013 01:19:00 GMT-0100', 
        'title': 'Surveillance program a test of Sino-US ties',
        'classname': 'chinadaily',
        'graph': '{"id":246,"edges":[{"id":"Hong KongCIA","weight":20,"source":"Hong Kong","target":"CIA","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"84","size":2,"x":"25"},{"id":"CIA","color":"rgb(102,153,255)","label":"CIA","y":"51","size":2,"x":"94"}]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[{"city":{"title":"Surveillance program a test of Sino-US ties","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}},{"city":{"title":"Surveillance program a test of Sino-US ties","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/china/2013-06/13/content_16612476.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Hong Kong, CIA'

        },
        {'start': 'Jun 12 2013 06:42:00 GMT-0100', 
        'title': 'NSA leaker is free man in HK-for now',
        'classname': 'chinadaily',
        'graph': '{"id":247,"edges":[{"id":"Hong KongEdward Snowden","weight":20,"source":"Hong Kong","target":"Edward Snowden","label":""}],"nodes":[{"id":"Hong Kong","color":"rgb(102,153,255)","label":"Hong Kong","y":"70","size":2,"x":"53"},{"id":"Edward Snowden","color":"rgb(102,153,255)","label":"Edward Snowden","y":"80","size":2,"x":"98"}]}',
        'emo': '[{"term":"freedom","value":2},{"term":"surveillance","value":1},{"term":"lawyer","value":1}]',
        'cities': '{"features":[{"city":{"title":"NSA leaker is free man in HK-for now","lon":114.1500015258789,"name":"Hong Kong","lat":22.283300399780273}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/12/content_16612175.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, , Hong Kong, Edward Snowden'

        },
        {'start': 'Jun 12 2013 03:45:00 GMT-0100', 
        'title': 'US group challenges NSA phone surveillance',
        'classname': 'chinadaily',
        'graph': '{"id":248,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/12/content_16611758.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: NSA, '

        },
        {'start': 'Jun 12 2013 03:38:00 GMT-0100', 
        'title': 'US tech firms push for govt transparency on security',
        'classname': 'chinadaily',
        'graph': '{"id":249,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/12/content_16611755.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Internet'

        },
        {'start': 'Jun 09 2013 10:26:00 GMT-0100', 
        'title': 'US attorney general under pressure to open more leak inquiries',
        'classname': 'chinadaily',
        'graph': '{"id":250,"edges":[],"nodes":[]}',
        'emo': '[{"term":"lawyer","value":1}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/09/content_16597655.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        },
        {'start': 'Jun 11 2013 09:34:00 GMT-0100', 
        'title': 'Russia may grant asylum for CIA whistleblower',
        'classname': 'chinadaily',
        'graph': '{"id":251,"edges":[{"id":"KremlinCIA","weight":20,"source":"Kremlin","target":"CIA","label":""},{"id":"KremlinRussia","weight":20,"source":"Kremlin","target":"Russia","label":""},{"id":"CIARussia","weight":20,"source":"CIA","target":"Russia","label":""}],"nodes":[{"id":"Kremlin","color":"rgb(102,153,255)","label":"Kremlin","y":"41","size":2,"x":"23"},{"id":"CIA","color":"rgb(102,153,255)","label":"CIA","y":"19","size":2,"x":"97"},{"id":"Russia","color":"rgb(102,153,255)","label":"Russia","y":"94","size":2,"x":"14"}]}',
        'emo': '[{"term":"asylum","value":2},{"term":"surveillance","value":1},{"term":"whistleblower","value":2}]',
        'cities': '{"features":[{"city":{"title":"Russia may grant asylum for CIA whistleblower","lon":-77.03250122070312,"name":"Washington","lat":38.874900817871094}}]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/11/content_16608774.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Kremlin, CIA, Russia'

        },
        {'start': 'Jun 08 2013 07:17:00 GMT-0100', 
        'title': 'Obama defends phone, Internet surveillance',
        'classname': 'chinadaily',
        'graph': '{"id":252,"edges":[],"nodes":[]}',
        'emo': '[{"term":"surveillance","value":2}]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/world/2013-06/08/content_16588754.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: , Barack Obama, Internet'

        },
        {'start': 'Jun 13 2013 07:57:00 GMT-0100', 
        'title': 'Unjustified US intelligence',
        'classname': 'chinadaily',
        'graph': '{"id":253,"edges":[],"nodes":[]}',
        'emo': '[]',
        'cities': '{"features":[]}',
        'description': '<a href=\"http://europe.chinadaily.com.cn/opinion/2013-06/13/content_16613028.htm\" target=\"_blank\">Go to article</a></br>Names mentioned: '

        }
]
};