//******************* GRAPH ************************
//******************* GRAPH ************************
//******************* GRAPH ************************

function dumpGraphData () {
	console.log ("----------------------");
	sigInst.iterNodes(
		function (n) {
			console.log (n.id + " x: " + n.x + " y: " + n.y);
		}
	);
	console.log ("");
}

function loadGraph (json) {
	if (json) {
		var parsed = $.parseJSON(json);
		var edges = parsed.edges;
	    var nodes = parsed.nodes;
	    var id = parsed.id;
	    if (graphsMap[id] == undefined)   {
		    graphsMap[id] = json;
	        $.each(nodes, function (index, value) {
	    		addNode(value.label, value.x, value.y, value.id, value.size, value.color);
	        });
	        $.each(edges, function (index, value) {
	            addEdge(value.source, value.target, value.id, value.weight);
	        });
	    	sigInst.draw();
	    	
		}
	}
}



function unloadGraph(json) {
	var parsed = $.parseJSON(json);
	var edges = parsed.edges;
	var nodes = parsed.nodes;
	var id = parsed.id;
	if ( id in graphsMap) {
		delete graphsMap[id];
		$.each(edges, function(index, value) {
			removeEdge(value.id);
		});
		$.each(nodes, function(index, value) {
			removeNode(value.id);
		});
		sigInst.draw();
		
	}
}



function addNode(label, x, y, id, size, color) {
	if (!sigInst.hasNode(id)) {
		sigInst.addNode(id, {
			'x' : x,
			'y' : y,
			label : label,
			size : size,
			color : color
		});
	} else {
		var nodeExisting = sigInst.getNodes(id);
		var exSize = parseInt(nodeExisting.size);
		exSize += parseInt(size);
		sigInst.setNodeSize(id, exSize);
		
	}
	sigInst.draw();
}



function removeNode(id) {
	if (sigInst.hasNode(id)) {
		var nodeExisting = sigInst.getNodes(id);
		var exSize = parseInt(nodeExisting.size);
		if (exSize <= 10) {
			sigInst.dropNode(id);
		} else {
			exSize--;
			sigInst.setNodeSize(id, exSize);
		}
	};
}



function removeEdge(id) {
	if (sigInst.hasEdge(id)) {
		var edgeExisting = sigInst.getEdges(id);
		var edgeweight = parseInt(edgeExisting.size);
		if (edgeweight == 10) {
			sigInst.dropEdge(id).draw();
		} else {
			sigInst.setEdgeWeight(id, edgeweight - 10);
		}
	}
}


function addEdge(source, target, id, weight) {
	if (!sigInst.hasEdge(id)) {
		sigInst.addEdge(id, source, target, weight).draw();
	} else {
		var edgeExisting = sigInst.getEdges(id);
		var exSize = parseInt(edgeExisting.size);
		exSize += parseInt(weight) + 10;
		sigInst.setEdgeWeight(id, exSize);
	}
}


function fa2Layout(time) {
	var myVar = setTimeout(myTimer, 0);
	function myTimer() {
		if (atlasAllowed) setTimeout(function() {sigInst.stopForceAtlas2();}, time);
		sigInst.startForceAtlas2();
	};
}




//****************** HELPER FUNCTIONS ********************
//****************** HELPER FUNCTIONS ********************
//****************** HELPER FUNCTIONS ********************

if ( typeof (Number.prototype.toRad) === "undefined") {
	Number.prototype.toRad = function() {
		return this * Math.PI / 180;
	};
}

if ( typeof (Number.prototype.toDeg) === "undefined") {
	Number.prototype.toDeg = function() {
		return this * 180 / Math.PI;
	};
}

function latLongToVector3(lon, lat, radius, heigth) {
	var phi = (lat) * Math.PI / 180;
	var theta = (lon - 180) * Math.PI / 180;
	var x = -(radius + heigth) * Math.cos(phi) * Math.cos(theta);
	var y = (radius + heigth) * Math.sin(phi);
	var z = (radius + heigth) * Math.cos(phi) * Math.sin(theta);
	return new THREE.Vector3(x, y, z);
}


Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key))
			size++;
	}
	return size;
};

var resizeTimerID = null;
function onResize() {
    if (resizeTimerID == null) {
        resizeTimerID = window.setTimeout(function() {
            resizeTimerID = null;
            tl.layout();
        }, 500);
    }
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
	WIDTH = $(window).width();
	HEIGHT = $(window).height() * 0.95;
	initCanvas ();
}



function themeSwitch(){
    var timeline1 = document.getElementById('tl');
    var cloud1 = document.getElementById('wordcloud');
    timeline1.className = (timeline1.className.indexOf('dark-theme') != -1) ? timeline1.className.replace('dark-theme', '') : timeline1.className += ' dark-theme';
}


function loadData() {
	var media = document.getElementById ("mediaList");
    var url = '.'; 
    
    sigInst.emptyGraph();
    eventSource1.clear();
    var selected = media.options[media.selectedIndex].text;
    ga('send', 'event', 'Select', selected, 'Overall', 1);
    eventSource1.loadJSON(mediaMap[selected], url);
    tl.layout(); // display the Timeline
    fa2Layout(3500);
}

function quickJump () {
	var jumps = document.getElementById ("jumpList");
	sigInst.emptyGraph();
	var selected = jumps.options[jumps.selectedIndex].text;
	console.log ("jump to " + jumpMap[selected]);
	jumpTo(jumpMap[selected]);
	fa2Layout(3500);
}

var playButton = document.getElementById("playButton");
var intervalID;
function playTimeline() {
	if (!playing) {
		playing = true;
		intervalID = setInterval (myTimer111, 3000);
		playButton.innerHTML = "Stop";
	}
	else {
		playing = false;
		window.clearInterval(intervalID);
		playButton.innerHTML = "Play";
	}
}

function myTimer111() {
	d = new Date ((d.getTime() + 21600000)); //10800000
	jumpTo(d);
};

var globeButton = document.getElementById("globeButton");
function stopGlobe() {
	if (!rotating) {
		rotating = true;
		globeButton.innerHTML = "Stop globe";		
	}	
	else {
		rotating = false;
		globeButton.innerHTML = "Start globe";	
	}
}

var helpButton = document.getElementById("helpButton");
var helpDiv = document.getElementById("help");
function showHelp() {
	if (!helpShown) {
		helpShown = true;
		helpButton.innerHTML = "Hide help";	
		helpDiv.style.visibility = "visible";	
	}	
	else {
		helpShown = false;
		helpButton.innerHTML = "Show help";	
		helpDiv.style.visibility = "hidden";	
	}
}

//********************* FLOT CHART ***********************
function drawChart (emoMap, emos) {
	//flot chart
	var chartData = [];
	for (var emotry in emoMap) {
		emos.push([emoMap[emotry], emotry]);
	}
	var series = {};
	series.color = "lightblue";
	series.data = emos;

	emos.sort(function(a, b) {
		if (a[0] > b[0])
			return 1;
		if (a[0] < b[0])
			return -1;
		return 0;
	});
	chartData.push(series);

	$(function() {
		$.plot("#placeholder", chartData, {
			series : {
				bars : {
					show : true,
					barWidth : 0.6,
					align : "center",
					fill : 1,
					horizontal : true
				}
			},
			yaxis : {
				mode : "categories",
				tickLength : 1
			},
			xaxis : {
				tickLength : 1
			}
		});
	});
}
